#[cfg(feature = "async")]
mod async_game;
pub mod state;

#[cfg(feature = "async")]
pub use async_game::AsyncGame;

use self::state::{GameCreateData, GameData};
use crate::error::ThreadedModelError;
use crate::model::{GenerationOptions, ThreadedModel, ThreadedTokenGenerator};
use std::ops::{Deref, DerefMut};

pub struct Game<'a> {
    model: &'a ThreadedModel,
    generation_options: GenerationOptions,
    generator: ThreadedTokenGenerator,
    data: GameData,
}

impl<'a> Game<'a> {
    pub fn new(
        create_data: GameCreateData,
        model: &'a ThreadedModel,
        mut generation_options: GenerationOptions,
    ) -> Result<Self, ThreadedModelError> {
        generation_options.output_full_sentences = true;
        let generator = Self::build_generator(model, String::from(""), generation_options)?;
        Ok(Self {
            model,
            generation_options,
            generator,
            data: GameData::new(create_data),
        })
    }

    pub fn model(&self) -> &'a ThreadedModel {
        self.model
    }

    pub fn add_input_line(&mut self, new_line: &str) -> Result<(), ThreadedModelError> {
        self.data
            .add_message(self.data.current_player_name().into(), new_line.into())
            .expect("BUG: wrong player name");
        self.generator = Self::build_generator(
            self.model,
            self.data.current_text().into(),
            self.generation_options,
        )?;
        Ok(())
    }

    pub fn next_token(&mut self) -> Option<String> {
        self.generator.next()
    }

    pub fn reset_generator(&mut self) -> Result<(), ThreadedModelError> {
        self.generator.reset()
    }

    fn build_generator(
        model: &ThreadedModel,
        text: String,
        generation_options: GenerationOptions,
    ) -> Result<ThreadedTokenGenerator, ThreadedModelError> {
        let generator = model.create_token_generator(text, generation_options)?;
        Ok(generator)
    }
}

impl<'a> Iterator for Game<'a> {
    type Item = String;

    fn next(&mut self) -> Option<String> {
        self.next_token()
    }
}

impl Deref for Game<'_> {
    type Target = GameData;

    fn deref(&self) -> &Self::Target {
        &self.data
    }
}

impl DerefMut for Game<'_> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.data
    }
}
