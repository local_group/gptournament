use crate::error::GptError;
use rust_bert::bart::{
    BartConfigResources, BartMergesResources, BartModelResources, BartVocabResources,
};
use rust_bert::pipelines::common::ModelType;
use rust_bert::pipelines::zero_shot_classification::{
    ZeroShotClassificationConfig, ZeroShotClassificationModel,
};
use rust_bert::resources::{RemoteResource, Resource};
use std::collections::HashMap;

pub struct Filter {
    model: ZeroShotClassificationModel,
}

impl Filter {
    pub fn new() -> Result<Self, GptError> {
        let config = ZeroShotClassificationConfig {
            model_type: ModelType::Bart,
            model_resource: Resource::Remote(RemoteResource::from_pretrained(
                BartModelResources::BART_MNLI,
            )),
            config_resource: Resource::Remote(RemoteResource::from_pretrained(
                BartConfigResources::BART_MNLI,
            )),
            vocab_resource: Resource::Remote(RemoteResource::from_pretrained(
                BartVocabResources::BART_MNLI,
            )),
            merges_resource: Some(Resource::Remote(RemoteResource::from_pretrained(
                BartMergesResources::BART_MNLI,
            ))),
            ..Default::default()
        };
        let model = ZeroShotClassificationModel::new(config).map_err(GptError::pipeline_error)?;

        Ok(Filter { model })
    }

    pub fn filter_value(&self, text: &str) -> f64 {
        let outputs = self
            .model
            .predict_multilabel(&[text], &["sex", "nudity", "adult content"], None, 128)
            .pop()
            .unwrap();

        let labels = outputs
            .iter()
            .map(|label| (label.text.as_str(), label.score))
            .collect::<HashMap<&str, f64>>();

        let filter_value = (labels.get("sex").unwrap() + 2. * labels.get("adult content").unwrap())
            / 3.
            + labels.get("nudity").unwrap();

        filter_value
    }
}
