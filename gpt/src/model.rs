#[cfg(feature = "async")]
mod async_model;
mod base_model;
mod processing;
pub(crate) mod threaded;

use crate::GptError;
#[cfg(feature = "async")]
pub use async_model::{AsyncModel, AsyncModelTokenGenerator};
pub use base_model::{GptTokenGenerator, Model, TextDecoder};
#[cfg(feature = "serde")]
use serde::{Deserialize, Serialize};
pub use threaded::{ThreadedModel, ThreadedTokenGenerator};

#[derive(Debug, Copy, Clone)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub enum ModelType {
    Gpt2XL,
    Gpt2Large,
    Gpt2Medium,
}

#[derive(Debug, Copy, Clone)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct GenerationOptions {
    pub top_k: i64,
    pub top_p: f64,
    pub temperature: f64,
    pub min_output_length: Option<i64>,
    pub max_output_length: Option<i64>,
    pub output_full_sentences: bool,
}

impl Default for GenerationOptions {
    fn default() -> Self {
        GenerationOptions {
            top_k: 40,
            top_p: 0.9,
            temperature: 1.0,
            max_output_length: Some(500),
            min_output_length: Some(50),
            output_full_sentences: true,
        }
    }
}

#[derive(Debug)]
pub enum ModelMessage {
    NewGenerator(String, GenerationOptions),
    DecodeTokens(Vec<i64>),
    GetTokenID(String),
    DropGenerator(i32),
    NextToken(i32),
    ResetGenerator(i32),
}

#[derive(Debug)]
pub enum ModelResult {
    ModelError(GptError),
    ModelLoaded(TextDecoder),
    Generator(i32),
    #[cfg(feature = "async")]
    StreamGenerator(i32, async_channel::Receiver<ModelResult>),
    Text(String),
    TokenID(i64),
    Token(Option<String>),
    Ok,
}
