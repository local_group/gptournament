use super::{
    GenerationOptions, GptTokenGenerator, Model, ModelMessage, ModelResult, ModelType, TextDecoder,
};
use crate::error::ThreadedModelError;
use std::collections::HashMap;
use std::sync::{mpsc, Mutex};
use std::thread::JoinHandle;

pub struct ThreadedModel {
    tx: mpsc::SyncSender<(ModelMessage, mpsc::SyncSender<ModelResult>)>,
    rx: Mutex<mpsc::Receiver<ModelResult>>,
    _handle: JoinHandle<Result<(), ThreadedModelError>>,
    model_loaded: bool,
    decoder: Option<TextDecoder>,
}

pub struct ThreadedTokenGenerator {
    id: i32,
    tx: mpsc::SyncSender<(ModelMessage, mpsc::SyncSender<ModelResult>)>,
}

struct InternalGenerator<'a> {
    generator: GptTokenGenerator<'a>,
}

impl ThreadedModel {
    pub fn new(model_type: ModelType) -> Self {
        log::debug!("Starting model thread");
        let (tx1, rx1) = mpsc::sync_channel(5);
        let (tx2, rx2) = mpsc::sync_channel(1);
        let handle = std::thread::spawn(move || model_thread(model_type, rx1, tx2));

        Self {
            tx: tx1,
            rx: Mutex::new(rx2),
            _handle: handle,
            model_loaded: false,
            decoder: None,
        }
    }

    pub fn wait_for_load(&mut self) -> Result<(), ThreadedModelError> {
        if self.model_loaded {
            return Ok(());
        }
        log::debug!("Waiting for model load");
        let msg = {
            let rx = self.rx.lock().unwrap();
            log_receive(rx.recv()?)
        };
        match msg {
            ModelResult::ModelLoaded(decoder) => {
                log::info!("Model loaded");
                self.model_loaded = true;
                self.decoder = Some(decoder);
                Ok(())
            }
            ModelResult::ModelError(e) => Err(e.into()),
            r => Err(ThreadedModelError::UnexpectedModelResult(r)),
        }
    }

    pub fn create_token_generator(
        &self,
        text: String,
        generation_options: GenerationOptions,
    ) -> Result<ThreadedTokenGenerator, ThreadedModelError> {
        self.ensure_model()?;
        log::debug!("Requested new generator");
        let msg = log_receive(self.send(log_send(ModelMessage::NewGenerator(
            text,
            generation_options,
        )))?);
        match msg {
            ModelResult::Generator(id) => {
                log::debug!("Created new generator with id {}", id);
                Ok(ThreadedTokenGenerator {
                    id,
                    tx: self.tx.clone(),
                })
            }
            r => Err(ThreadedModelError::UnexpectedModelResult(r)),
        }
    }

    pub fn decode_text(&self, tokens: Vec<i64>) -> Result<String, ThreadedModelError> {
        self.ensure_model()?;
        Ok(self.decoder.as_ref().unwrap().decode_text(tokens))
    }

    pub fn get_token_id(&self, token: &str) -> Result<i64, ThreadedModelError> {
        self.ensure_model()?;
        Ok(self.decoder.as_ref().unwrap().get_token_id(token))
    }

    fn ensure_model(&self) -> Result<(), ThreadedModelError> {
        if self.model_loaded {
            Ok(())
        } else {
            Err(ThreadedModelError::ModelNotLoaded)
        }
    }

    fn send(&self, msg: ModelMessage) -> Result<ModelResult, ThreadedModelError> {
        let (tx, rx) = mpsc::sync_channel(1);
        self.tx.send((msg, tx))?;
        Ok(rx.recv()?)
    }
}

impl ThreadedTokenGenerator {
    pub fn next_token(&self) -> Result<Option<String>, ThreadedModelError> {
        let msg = log_receive(self.send(log_send(ModelMessage::NextToken(self.id)))?);
        match msg {
            ModelResult::Token(token) => Ok(token),
            r => Err(ThreadedModelError::UnexpectedModelResult(r)),
        }
    }

    pub fn reset(&self) -> Result<(), ThreadedModelError> {
        let msg = log_receive(self.send(log_send(ModelMessage::ResetGenerator(self.id)))?);
        match msg {
            ModelResult::Ok => Ok(()),
            r => Err(ThreadedModelError::UnexpectedModelResult(r)),
        }
    }

    fn send(&self, msg: ModelMessage) -> Result<ModelResult, ThreadedModelError> {
        let (tx, rx) = mpsc::sync_channel(1);
        self.tx.send((msg, tx))?;
        Ok(rx.recv()?)
    }
}

impl Iterator for ThreadedTokenGenerator {
    type Item = String;

    fn next(&mut self) -> Option<Self::Item> {
        self.next_token().ok().flatten()
    }
}

impl Drop for ThreadedTokenGenerator {
    fn drop(&mut self) {
        let _ = self.send(ModelMessage::DropGenerator(self.id));
    }
}

fn model_thread(
    model_type: ModelType,
    rx: mpsc::Receiver<(ModelMessage, mpsc::SyncSender<ModelResult>)>,
    tx: mpsc::SyncSender<ModelResult>,
) -> Result<(), ThreadedModelError> {
    let model = match Model::new(model_type) {
        Ok(model) => model,
        Err(err) => {
            tx.send(log_send(ModelResult::ModelError(err)))?;
            return Ok(());
        }
    };
    tx.send(log_send(ModelResult::ModelLoaded(model.get_decoder())))?;
    drop(tx);
    let model = &model;

    let mut generators = HashMap::new();
    let mut generator_id = 0;

    while let Ok((msg, tx)) = log_receive(rx.recv()) {
        match msg {
            ModelMessage::NewGenerator(text, opts) => {
                let generator = model.create_token_generator_from_text(&text, opts);
                generators.insert(generator_id, InternalGenerator { generator });
                tx.send(log_send(ModelResult::Generator(generator_id)))?;
                generator_id += 1;
            }
            ModelMessage::DropGenerator(id) => {
                generators.remove(&id);
            }
            ModelMessage::NextToken(id) => {
                if let Some(generator) = generators.get_mut(&id) {
                    tx.send(log_send(ModelResult::Token(generator.generator.next())))?;
                }
            }
            ModelMessage::DecodeTokens(tokens) => {
                tx.send(log_send(ModelResult::Text(model.decode_text(tokens))))?;
            }
            ModelMessage::GetTokenID(token) => {
                tx.send(log_send(ModelResult::TokenID(model.get_token_id(&token))))?;
            }
            ModelMessage::ResetGenerator(id) => {
                if let Some(generator) = generators.get_mut(&id) {
                    generator.generator.reset();
                    tx.send(log_send(ModelResult::Ok))?;
                }
            }
        }
    }

    Ok(())
}

fn log_receive<T: std::fmt::Debug>(msg: T) -> T {
    log::trace!("received message: {:?}", msg);
    msg
}

fn log_send<T: std::fmt::Debug>(msg: T) -> T {
    log::trace!("sending message: {:?}", msg);
    msg
}
