use super::{GenerationOptions, TextDecoder};

pub struct TokenProcessor {
    min_length: Option<i64>,
    max_length: Option<i64>,
    use_full_sentences: bool,
    current_length: i64,
    is_at_end: bool,
    sentence_end_tokens: Vec<i64>,
}

impl TokenProcessor {
    pub fn from_generation_options(options: GenerationOptions, decoder: &TextDecoder) -> Self {
        let sentence_end_tokens = vec![".", "!", "?", "\u{010a}", ".\"", "!\"", "?\"", "\"."]
            .into_iter()
            .map(|token| decoder.get_token_id(token))
            .collect();

        Self {
            min_length: options.min_output_length,
            max_length: options.max_output_length,
            use_full_sentences: options.output_full_sentences,
            current_length: 0,
            is_at_end: false,
            sentence_end_tokens,
        }
    }

    fn next_min_length_token<F: FnMut(&mut Self) -> Option<i64>>(
        &mut self,
        mut f: F,
    ) -> Option<i64> {
        if let Some(min_length) = self.min_length {
            if self.current_length >= min_length {
                f(self)
            } else {
                match f(self) {
                    Some(token) => Some(token),
                    None => self.next_min_length_token(f),
                }
            }
        } else {
            f(self)
        }
    }

    fn next_max_length_token<F: FnMut(&mut Self) -> Option<i64>>(
        &mut self,
        mut f: F,
    ) -> Option<i64> {
        if let Some(max_length) = self.max_length {
            if self.current_length > max_length {
                None
            } else {
                f(self)
            }
        } else {
            f(self)
        }
    }

    fn next_full_sentence_token<F: FnMut(&mut Self) -> Option<i64>>(
        &mut self,
        mut f: F,
    ) -> Option<i64> {
        if self.use_full_sentences {
            if self.is_at_end {
                self.is_at_end = false;
                return None;
            }

            let token = f(self);
            match token {
                None => self.next_full_sentence_token(f),
                Some(token) => {
                    if self.sentence_end_tokens.contains(&token) {
                        self.is_at_end = true;
                    } else {
                        self.is_at_end = false;
                    }
                    Some(token)
                }
            }
        } else {
            f(self)
        }
    }

    pub fn next_token<T: FnMut() -> Option<i64>>(&mut self, mut cbk: T) -> Option<i64> {
        let token = self.next_max_length_token(|m| {
            m.next_min_length_token(|m| m.next_full_sentence_token(|_m| cbk()))
        });
        if token.is_some() {
            self.current_length += 1;
        }
        token
    }

    pub fn reset(&mut self) {
        self.is_at_end = false;
        self.current_length = 0;
    }
}

pub struct TokenDecoder {
    decoder: TextDecoder,
    stored_tokens: Vec<i64>,
}

impl TokenDecoder {
    pub fn new(decoder: TextDecoder) -> Self {
        Self {
            decoder,
            stored_tokens: Vec::new(),
        }
    }

    pub fn decode(&mut self, next_token: i64) -> Option<String> {
        self.stored_tokens.push(next_token);
        let text = self.decoder.decode_text(self.stored_tokens.clone());
        // continue decoding if we have an invalid codepoint in the output
        if text.contains('\u{fffd}') {
            None
        } else {
            self.stored_tokens.clear();
            Some(text)
        }
    }

    pub fn next_token<T: FnMut(&mut S) -> Option<i64>, S>(
        &mut self,
        mut generator: T,
        state: &mut S,
    ) -> Option<String> {
        loop {
            match generator(state) {
                None => return None,
                Some(token) => {
                    if let Some(token) = self.decode(token) {
                        return Some(token);
                    }
                }
            }
        }
    }
}
