use super::{GameCreateData, GameData};
use crate::error::ThreadedModelError;
use crate::model::{AsyncModel, AsyncModelTokenGenerator, GenerationOptions};
use futures_lite::{Stream, StreamExt};
use std::ops::{Deref, DerefMut};
use std::{
    pin::Pin,
    task::{Context, Poll},
};

pub struct AsyncGame {
    model: AsyncModel,
    generation_options: GenerationOptions,
    generator: Option<AsyncModelTokenGenerator>,
    data: GameData,
}

impl AsyncGame {
    pub async fn new(
        create_data: GameCreateData,
        model: &AsyncModel,
        mut generation_options: GenerationOptions,
    ) -> Result<Self, ThreadedModelError> {
        generation_options.output_full_sentences = true;
        Ok(Self {
            model: model.clone(),
            generation_options,
            generator: None,
            data: GameData::new(create_data),
        })
    }

    pub async fn add_input_line(&mut self, new_line: &str) -> Result<(), ThreadedModelError> {
        self.data
            .add_message(self.data.current_player_name().into(), new_line.into())
            .expect("BUG: wrong player name");
        self.generator = Some(
            Self::build_generator(
                &self.model,
                self.data.current_text().into(),
                self.generation_options,
            )
            .await?,
        );
        Ok(())
    }

    pub async fn next_token(&mut self) -> Option<String> {
        self.generator.as_mut().unwrap().next().await
    }

    pub async fn reset_generator(&mut self) -> Result<(), ThreadedModelError> {
        self.generator.as_mut().unwrap().reset().await
    }

    async fn build_generator(
        model: &AsyncModel,
        text: String,
        generation_options: GenerationOptions,
    ) -> Result<AsyncModelTokenGenerator, ThreadedModelError> {
        let generator = model
            .create_token_generator(text, generation_options)
            .await?;
        Ok(generator)
    }
}

impl Deref for AsyncGame {
    type Target = GameData;

    fn deref(&self) -> &Self::Target {
        &self.data
    }
}

impl DerefMut for AsyncGame {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.data
    }
}

impl Stream for AsyncGame {
    type Item = String;

    fn poll_next(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        match self.generator.as_mut().unwrap().poll_next(cx) {
            Poll::Pending => Poll::Pending,
            Poll::Ready(item) => Poll::Ready(item),
        }
    }
}
