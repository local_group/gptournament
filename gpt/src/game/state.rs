use crate::error::GameError;
use std::collections::HashMap;
use std::ops::Deref;

#[derive(Debug, Clone)]
pub struct GameCreateData {
    players: Vec<String>,
}

#[derive(Debug, Clone)]
pub struct GameData {
    text: String,
    history: Vec<GameItem>,
    players: HashMap<String, PlayerData>,
    player_order: Vec<String>,
    current_player: usize,
    players_alive: usize,
}

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub enum PlayerState {
    Alive,
    Dead,
}

#[derive(Debug, Clone)]
pub struct PlayerData {
    state: PlayerState,
}

#[derive(Debug, Clone)]
pub enum GameItem {
    Message(Message),
    Kill(Kill),
    Revive(Revive),
}

#[derive(Debug, Clone)]
pub struct Message {
    text: String,
    sender: String,
    timestamp: chrono::DateTime<chrono::Local>,
    responses: Vec<GptResponse>,
}

#[derive(Debug, Clone)]
pub struct GptResponse {
    text: String,
    timestamp: chrono::DateTime<chrono::Local>,
}

#[derive(Debug, Clone)]
pub struct Kill {
    player: String,
}

#[derive(Debug, Clone)]
pub struct Revive {
    player: String,
}

#[derive(Debug)]
pub struct PlayerRef<'a> {
    name: String,
    data: &'a mut PlayerData,
    players_alive: &'a mut usize,
    history: &'a mut Vec<GameItem>,
    player_order: &'a mut Vec<String>,
    current_player: &'a mut usize,
}

impl Default for GameCreateData {
    fn default() -> Self {
        Self::new()
    }
}

impl GameCreateData {
    pub fn new() -> Self {
        Self {
            players: Vec::new(),
        }
    }

    pub fn add_player(&mut self, name: String) -> Result<(), GameError> {
        if self.players.contains(&name) {
            return Err(GameError::PlayerExists(name));
        }
        self.players.push(name);
        Ok(())
    }

    pub fn remove_player(&mut self, name: String) -> Result<(), GameError> {
        if !self.players.contains(&name) {
            return Err(GameError::UnknownPlayer(name));
        }
        self.players.remove(
            self.players
                .iter()
                .enumerate()
                .find(|(_, p)| p == &&name)
                .unwrap()
                .0,
        );
        Ok(())
    }

    pub fn move_player_down(&mut self, name: String) -> Result<(), GameError> {
        if !self.players.contains(&name) {
            return Err(GameError::UnknownPlayer(name));
        }
        let index = self
            .players
            .iter()
            .enumerate()
            .find(|(_, p)| p == &&name)
            .unwrap()
            .0;
        let index2 = (index + self.players.len() - 1) % self.players.len();
        self.players.swap(index, index2);
        Ok(())
    }

    pub fn move_player_up(&mut self, name: String) -> Result<(), GameError> {
        if !self.players.contains(&name) {
            return Err(GameError::UnknownPlayer(name));
        }
        let index = self
            .players
            .iter()
            .enumerate()
            .find(|(_, p)| p == &&name)
            .unwrap()
            .0;
        let index2 = (index + 1) % self.players.len();
        self.players.swap(index, index2);
        Ok(())
    }

    pub fn players(&self) -> &Vec<String> {
        &self.players
    }

    pub fn players_mut(&mut self) -> &mut Vec<String> {
        &mut self.players
    }
}

impl GameData {
    pub fn new(data: GameCreateData) -> Self {
        let mut players = HashMap::new();
        for player in data.players.iter() {
            players.insert(
                player.clone(),
                PlayerData {
                    state: PlayerState::Alive,
                },
            );
        }
        let players_alive = data.players.len();
        let mut initial_text = String::new();
        for (i, player) in data.players.iter().enumerate() {
            initial_text.push_str(player);
            if i + 2 < players_alive {
                initial_text.push_str(", ");
            } else if i + 1 < players_alive {
                initial_text.push_str(" and ");
            }
        }
        initial_text.push_str(" are playing a game.");
        log::debug!("Initial text");
        Self {
            text: initial_text,
            history: Vec::new(),
            players,
            player_order: data.players,
            current_player: 0,
            players_alive,
        }
    }

    pub fn player_by_name(&mut self, name: String) -> Result<PlayerRef, GameError> {
        if let Some(data) = self.players.get_mut(&name) {
            Ok(PlayerRef {
                name,
                data,
                players_alive: &mut self.players_alive,
                history: &mut self.history,
                player_order: &mut self.player_order,
                current_player: &mut self.current_player,
            })
        } else {
            Err(GameError::UnknownPlayer(name))
        }
    }

    pub fn current_player_name(&self) -> &str {
        self.player_order.get(self.current_player).unwrap()
    }

    pub fn current_player(&mut self) -> PlayerRef {
        let name = String::from(self.current_player_name());
        self.player_by_name(name).unwrap()
    }

    pub fn next_player(&mut self) -> PlayerRef {
        if self.players_alive <= 1 {
            return self.current_player();
        }
        self.current_player = (self.current_player + 1) % self.players.len();
        if self.current_player().is_dead() {
            self.next_player()
        } else {
            self.current_player()
        }
    }

    pub fn current_text(&self) -> &str {
        &self.text
    }

    pub fn add_message(&mut self, player: String, text: String) -> Result<(), GameError> {
        if player != self.current_player_name() {
            return Err(GameError::WrongPlayer(player));
        }
        self.text.push_str(&text);
        self.history.push(GameItem::Message(Message {
            sender: player,
            text,
            timestamp: chrono::Local::now(),
            responses: Vec::new(),
        }));
        Ok(())
    }

    pub fn add_gpt_response(&mut self, text: String) {
        let h = self
            .history
            .iter_mut()
            .rfind(|h| matches!(h, GameItem::Message(_)));
        if let Some(GameItem::Message(msg)) = h {
            msg.responses.push(GptResponse {
                text,
                timestamp: chrono::Local::now(),
            })
        }
    }

    pub fn history(&self) -> &Vec<GameItem> {
        &self.history
    }

    pub fn history_mut(&mut self) -> &mut Vec<GameItem> {
        &mut self.history
    }

    pub fn current_player_index(&self) -> usize {
        self.current_player
    }

    pub fn set_current_player(&mut self, current_player: usize) {
        let current_player = current_player % self.players.len();
        let alive_indices = self
            .player_order
            .iter()
            .enumerate()
            .filter_map(|(i, p)| {
                let s = self.players.get(p).unwrap();
                if s.is_alive() {
                    Some(i)
                } else {
                    None
                }
            })
            .collect::<Vec<_>>();
        if alive_indices.is_empty() {
            // no alive players -> do nothing
            return;
        }
        // try to set the current player to any alive player after the current player
        for i in alive_indices.iter() {
            if current_player <= *i {
                self.current_player = *i;
                return;
            }
        }
        // now set the current player to the first alive player (wrap around)
        self.current_player = alive_indices[0]
    }

    pub fn players(&self) -> &Vec<String> {
        &self.player_order
    }

    pub fn alive_player_count(&self) -> usize {
        self.players_alive
    }

    pub fn alive_players(&self) -> Vec<String> {
        self.player_order
            .iter()
            .filter(|p| self.players.get(*p).unwrap().is_alive())
            .cloned()
            .collect()
    }

    pub fn end_game(&mut self) {
        for (_, d) in self.players.iter_mut() {
            d.state = PlayerState::Dead;
        }
        self.players_alive = 0;
    }

    pub fn winner(&mut self) -> Option<PlayerRef> {
        if self.players_alive != 1 {
            return None;
        }
        let mut alive_players = self
            .players
            .iter_mut()
            .filter(|p| p.1.is_alive())
            .collect::<Vec<_>>();
        if alive_players.len() != 1 {
            None
        } else {
            let (name, data) = alive_players.pop().unwrap();
            Some(PlayerRef {
                name: name.clone(),
                data,
                players_alive: &mut self.players_alive,
                history: &mut self.history,
                player_order: &mut self.player_order,
                current_player: &mut self.current_player,
            })
        }
    }
}

impl PlayerData {
    pub fn state(&self) -> PlayerState {
        self.state
    }

    pub fn is_alive(&self) -> bool {
        self.state == PlayerState::Alive
    }

    pub fn is_dead(&self) -> bool {
        self.state == PlayerState::Dead
    }
}

impl Deref for PlayerRef<'_> {
    type Target = PlayerData;

    fn deref(&self) -> &Self::Target {
        self.data
    }
}

impl PlayerRef<'_> {
    pub fn name(&self) -> &str {
        &self.name
    }

    pub fn state(&self) -> PlayerState {
        self.data.state()
    }

    pub fn is_alive(&self) -> bool {
        self.data.is_alive()
    }

    pub fn is_dead(&self) -> bool {
        self.data.is_dead()
    }

    pub fn kill(&mut self) {
        if self.is_alive() {
            self.data.state = PlayerState::Dead;
            *self.players_alive -= 1;
            self.history.push(GameItem::Kill(Kill {
                player: self.name.clone(),
            }))
        }
    }

    pub fn revive(&mut self) {
        if self.is_dead() {
            self.data.state = PlayerState::Alive;
            *self.players_alive += 1;
            self.history.push(GameItem::Revive(Revive {
                player: self.name.clone(),
            }))
        }
    }
}

impl Message {
    pub fn text(&self) -> &str {
        &self.text
    }

    pub fn sender(&self) -> &str {
        &self.sender
    }

    pub fn timestamp(&self) -> &chrono::DateTime<chrono::Local> {
        &self.timestamp
    }

    pub fn responses(&self) -> &Vec<GptResponse> {
        &self.responses
    }

    pub fn add_response(&mut self, response: GptResponse) {
        self.responses.push(response);
    }
}

impl GptResponse {
    pub fn new(text: String, timestamp: chrono::DateTime<chrono::Local>) -> Self {
        Self { timestamp, text }
    }

    pub fn text(&self) -> &str {
        &self.text
    }

    pub fn timestamp(&self) -> &chrono::DateTime<chrono::Local> {
        &self.timestamp
    }
}

impl Kill {
    pub fn player(&self) -> &str {
        &self.player
    }
}

impl Revive {
    pub fn player(&self) -> &str {
        &self.player
    }
}
