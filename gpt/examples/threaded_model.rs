use gpt::model::{GenerationOptions, ModelType, ThreadedModel};

use std::io::{self, Write};

fn main() {
    gpt::set_model_path_to_cwd();

    let text = "Hello world!";
    let model = {
        let mut model = ThreadedModel::new(ModelType::Gpt2Large);
        model.wait_for_load().unwrap();
        model
    };

    let options = GenerationOptions::default();

    let generator = model.create_token_generator(text.into(), options).unwrap();
    print!("{}", text);
    for token in generator {
        print!("{}", token);
        let _ = io::stdout().flush();
    }
}
