use gpt::model::{GenerationOptions, Model, ModelType};

use std::io::{self, Write};

fn main() {
    gpt::set_model_path_to_cwd();

    let text = "Hello world!";
    let model = Model::new(ModelType::Gpt2Large).unwrap();

    let options = GenerationOptions::default();

    let generator = model.create_token_generator_from_text(text, options);
    print!("{}", text);
    for token in generator {
        print!("{}", token);
        let _ = io::stdout().flush();
    }
}
