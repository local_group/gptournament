# GPTournament 2

This project contains a GPTournament 2 backend written in Rust and a web API to access it.

## Running in Docker

Example `docker-compose.yml` file:

```yaml
version: '3'
services:
  gptournament:
    image: registry.gitlab.com/local_group/gptournament
    command: -v --model=/model
    volumes:
      - ./model:/model
      - ./config.toml:/app/config.toml
```

## API docs

[GPT REST API docs](docs/api.md)
