use anyhow::Result;
use sass_rs::{Context, Options, OutputStyle};
use std::fs::File;
use std::io::Write;
use std::path::Path;

fn main() {
    if let Err(err) = main_result() {
        panic!(format!("{:#?}", err));
    }
}

fn main_result() -> Result<()> {
    let mut context = Context::new_file("sass/style.scss").map_err(|s| anyhow::anyhow!(s))?;
    context.set_options(Options {
        include_paths: Vec::new(),
        indented_syntax: true,
        output_style: OutputStyle::Compressed,
        precision: 2,
    });
    let content = context.compile().map_err(|s| anyhow::anyhow!(s))?;
    let mut file = File::create("style.css")?;
    file.write_all(content.as_bytes())?;

    println!("cargo:rerun-if-changed=build.rs");
    print_files("sass")?;
    Ok(())
}

fn print_files<P: AsRef<Path>>(path: P) -> Result<()> {
    for file in std::fs::read_dir(&path)? {
        let file = file?;
        if file.path().is_dir() {
            print_files(file.path())?;
        } else if file.path().is_file() {
            println!("cargo:rerun-if-changed={}", file.path().to_str().unwrap());
        }
    }

    Ok(())
}
