use crate::Msg;
use chrono::TimeZone;
use seed::prelude::*;
use serde::de::DeserializeOwned;
use web_common::types::response;
use web_sys::{DragEvent, Event, KeyboardEvent};

pub type FetchResult<T> = Result<T, FetchError>;

#[derive(Debug, Clone)]
pub struct FetchError {
    message: String,
}

impl FetchError {
    pub fn from(e: seed::fetch::FetchError) -> Self {
        Self {
            message: format!("{:?}", e),
        }
    }
}

pub async fn fetch_response<T: DeserializeOwned + 'static, F: FnOnce(FetchResult<T>) -> Msg>(
    request: Request<'_>,
    f: F,
) -> Msg {
    async fn fetch<T: DeserializeOwned + 'static>(
        request: Request<'_>,
    ) -> Result<T, seed::fetch::FetchError> {
        Ok(request.fetch().await?.json().await?)
    }

    f(fetch(request).await.map_err(FetchError::from))
}

pub fn handle_other_msg(
    m: Msg,
    global: &mut crate::model::GlobalData,
    orders: &mut impl Orders<Msg>,
) {
    match m {
        Msg::CreateLobbyFetched(FetchResult::Ok(response::Response::Error { message }))
        | Msg::CreateLobbyFetched(FetchResult::Err(FetchError { message }))
        | Msg::LoginFetched(FetchResult::Ok(response::Response::Error { message }))
        | Msg::LoginFetched(FetchResult::Err(FetchError { message }))
        | Msg::UserInfoFetched(FetchResult::Ok(response::Response::Error { message }))
        | Msg::UserInfoFetched(FetchResult::Err(FetchError { message })) => {
            let msg = format!("Error fetching resource: {}", message);
            seed::error!(msg);
            orders.send_msg(Msg::ErrorMessage(msg));
        }
        Msg::UserInfoFetched(FetchResult::Ok(response::UserTypeResponse::Ok(user))) => match user {
            response::UserType::NotLoggedIn | response::UserType::ApiKey => {
                global.remove_user();
            }
            response::UserType::User(user_info) => {
                if let Some(ref mut user) = global.user {
                    user.username = user_info.username;
                }
            }
        },
        _ => {}
    }
}

pub trait IntoDragEvent {
    fn into_drag_event(self) -> DragEvent;
}

impl IntoDragEvent for Event {
    fn into_drag_event(self) -> DragEvent {
        self.dyn_into::<web_sys::DragEvent>()
            .expect("cannot cast given event into DragEvent")
    }
}

pub trait IntoKeyboardEvent {
    fn into_keyboard_event(self) -> KeyboardEvent;
}

impl IntoKeyboardEvent for Event {
    fn into_keyboard_event(self) -> KeyboardEvent {
        self.dyn_into::<web_sys::KeyboardEvent>()
            .expect("cannot cast given event into DragEvent")
    }
}

pub fn get_date_time() -> chrono::DateTime<chrono::Local> {
    let milliseconds = stdweb::web::Date::now();
    let seconds = (milliseconds / 1000.0).trunc() as i64;
    let nanoseconds = ((milliseconds * 1_000_000.0) % 1_000_000_000.0).trunc() as u32;
    chrono::Local.timestamp(seconds, nanoseconds)
}

pub fn render_date_time(tm: &web_common::types::websocket::Timestamp) -> String {
    tm.format("%F %R").to_string()
}
