mod connected;

use self::connected::ConnectedModel;
use crate::{api, util::FetchResult};
use chrono::Timelike;
use seed::{browser::web_socket::WebSocket, prelude::*, *};
use serde::{Deserialize, Serialize};
use web_common::types::{response, websocket::ServerMessage};

#[derive(Debug, Clone)]
pub enum Msg {
    ToLoginPageClicked,
    UsernameChanged(String),
    PasswordChanged(String),
    LobbyTokenChanged(String),
    LobbyNameChanged(String),
    MessageTextChanged(String),
    StayLoggedInClicked,
    LogoutClicked,
    LoginClicked,
    JoinLobbyClicked,
    CreateLobbyClicked,
    StartGameClicked,
    SubmitMessageClicked,
    ReviveClicked(String),
    KillClicked(String),
    LoginFetched(FetchResult<response::TokenResponse>),
    CreateLobbyFetched(FetchResult<response::TokenResponse>),
    UserInfoFetched(FetchResult<response::UserTypeResponse>),
    ReorderPlayer {
        source_player: String,
        target_player: String,
        after: bool,
    },
    SendTypingNotificationTimeout,
    ReceivedTypingNotificationTimeout(String),
    ScrollMessagesToBottom,
    ErrorMessage(String),
    WsError,
    WsServerMessageReceived(ServerMessage),
    Dummy,
}

#[derive(Debug)]
pub struct Model {
    page: ModelPage,
    stack: Vec<ModelPage>,
    global: GlobalData,
}

#[derive(Debug)]
pub struct GlobalData {
    pub user: Option<UserData>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct UserData {
    pub token: String,
    pub username: String,
}

pub enum UpdateResult {
    Ok,
    Push(ModelPage),
    Pop,
    Replace(ModelPage),
}

#[derive(Debug)]
pub enum ModelPage {
    Disconnected(DisconnectedModel),
    Connecting(ConnectingModel),
    Connected(ConnectedModel),
    Login(LoginModel),
}

pub trait PageView {
    fn view_nav_left(&self, global: &GlobalData) -> Vec<Node<Msg>> {
        vec![]
    }

    fn view_nav_right(&self, global: &GlobalData) -> Vec<Node<Msg>> {
        vec![]
    }

    fn view(&self, global: &GlobalData) -> Node<Msg> {
        empty![]
    }
}

pub trait Page<O: Orders<Msg>>: PageView {
    fn update(&mut self, global: &mut GlobalData, msg: Msg, orders: &mut O) -> UpdateResult;
}

#[derive(Debug)]
pub struct DisconnectedModel {
    username: String,
    lobby_token: String,
    lobby_name: String,
    error: Option<String>,
}

#[derive(Debug)]
pub struct ConnectingModel {
    username: String,
    lobby_token: String,
    ws: Option<WebSocket>,
}

#[derive(Debug)]
pub struct LoginModel {
    username: String,
    password: String,
    stay_logged_in: bool,
}

impl Model {
    pub fn new(orders: &mut impl Orders<Msg>) -> Self {
        orders.after_next_render(|_| Msg::Dummy);
        Self {
            page: ModelPage::Disconnected(DisconnectedModel::new()),
            stack: Vec::new(),
            global: GlobalData::new(orders),
        }
    }

    pub fn update<O: Orders<Msg>>(&mut self, msg: Msg, orders: &mut O) {
        let page: &mut dyn Page<_> = match self.page {
            ModelPage::Disconnected(ref mut p) => p,
            ModelPage::Connecting(ref mut p) => p,
            ModelPage::Connected(ref mut p) => p,
            ModelPage::Login(ref mut p) => p,
        };
        match page.update(&mut self.global, msg, orders) {
            UpdateResult::Ok => {}
            UpdateResult::Pop => {
                self.page = self.stack.pop().unwrap();
            }
            UpdateResult::Push(mut new_page) => {
                std::mem::swap(&mut self.page, &mut new_page);
                self.stack.push(new_page);
            }
            UpdateResult::Replace(new_page) => {
                self.page = new_page;
            }
        }
        let now = crate::util::get_date_time();
        let timeout = 60 - now.second();
        orders.perform_cmd(cmds::timeout(timeout, || Msg::Dummy));
    }

    pub fn view(&self) -> Vec<Node<Msg>> {
        let page: &dyn PageView = match self.page {
            ModelPage::Disconnected(ref p) => p,
            ModelPage::Connecting(ref p) => p,
            ModelPage::Connected(ref p) => p,
            ModelPage::Login(ref p) => p,
        };
        let footer_text = format!("© {} — ", 2021);
        let mut nav_left = vec![h1![attrs! { At::Class => "header" }, "GPTournament 2"]];
        for n in page.view_nav_left(&self.global) {
            nav_left.push(n);
        }
        let now = crate::util::get_date_time();
        vec![
            nav![div![nav_left], div![page.view_nav_right(&self.global)],],
            main![page.view(&self.global)],
            footer![
                p![
                    attrs! { At::Class => "flex-grow" },
                    footer_text,
                    a![
                        attrs! { At::Href => "https://999eagle.moe/" },
                        "Sophie Tauchert"
                    ],
                ],
                p![now.format("%R").to_string()],
            ],
        ]
    }
}

impl GlobalData {
    fn new(orders: &mut impl Orders<Msg>) -> Self {
        let user: Option<UserData> = LocalStorage::get("user")
            .ok()
            .or_else(|| SessionStorage::get("user").ok());
        if let Some(ref user) = user {
            let url = api::get_api_endpoint(api::API_USER_INFO);
            let request = Request::new(url).header(Header::bearer(user.token.clone()));
            orders.perform_cmd(crate::util::fetch_response(request, Msg::UserInfoFetched));
        }
        Self { user }
    }

    pub fn set_user(&mut self, user: UserData, persistent: bool) {
        if persistent {
            let _ = LocalStorage::insert("user", &user);
        } else {
            let _ = SessionStorage::insert("user", &user);
        }
        self.user.replace(user);
    }

    pub fn remove_user(&mut self) {
        self.user.take();
        let _ = LocalStorage::remove("user");
        let _ = LocalStorage::remove("user");
    }
}

impl DisconnectedModel {
    fn new() -> Self {
        Self {
            username: "".into(),
            lobby_token: "".into(),
            lobby_name: "".into(),
            error: None,
        }
    }
}

impl<O: Orders<Msg>> Page<O> for DisconnectedModel {
    fn update(&mut self, global: &mut GlobalData, msg: Msg, orders: &mut O) -> UpdateResult {
        match msg {
            Msg::ToLoginPageClicked => {
                return UpdateResult::Push(ModelPage::Login(LoginModel::new()));
            }
            Msg::LogoutClicked => {
                global.remove_user();
            }
            Msg::LobbyTokenChanged(v) => self.lobby_token = v,
            Msg::UsernameChanged(v) => self.username = v,
            Msg::LobbyNameChanged(v) => self.lobby_name = v,
            Msg::JoinLobbyClicked => {
                let url = api::get_ws_url(&self.username, &self.lobby_token);
                let ws = seed::browser::web_socket::WebSocket::builder(url, orders)
                    .on_message(|msg| match msg.json::<ServerMessage>() {
                        Ok(msg) => Msg::WsServerMessageReceived(msg),
                        Err(e) => {
                            error!(format!(
                                "Error parsing incoming web socket message: {:?}",
                                e
                            ));
                            Msg::Dummy
                        }
                    })
                    .on_error(|| Msg::WsError)
                    .build_and_open();
                match ws {
                    Ok(ws) => {
                        return UpdateResult::Push(ModelPage::Connecting(ConnectingModel::new(
                            ws,
                            self.username.clone(),
                            self.lobby_token.clone(),
                        )));
                    }
                    Err(e) => {
                        error!(format!("Error connecting to server: {:?}", e));
                    }
                }
            }
            Msg::CreateLobbyClicked => {
                let params = api::Name {
                    name: self.lobby_name.clone(),
                };
                let url = api::get_api_get_endpoint(api::API_CREATE_LOBBY, params);
                let request = Request::new(url)
                    .header(Header::bearer(global.user.as_ref().unwrap().token.clone()));
                orders.perform_cmd(crate::util::fetch_response(
                    request,
                    Msg::CreateLobbyFetched,
                ));
            }
            Msg::CreateLobbyFetched(FetchResult::Ok(response::TokenResponse::Ok(token))) => {
                self.lobby_token = token.token;
            }
            Msg::ErrorMessage(error) => {
                self.error.replace(error);
            }
            m => crate::util::handle_other_msg(m, global, orders),
        }
        UpdateResult::Ok
    }
}

impl PageView for DisconnectedModel {
    fn view_nav_right(&self, global: &GlobalData) -> Vec<Node<Msg>> {
        let mut nodes = Vec::new();
        if let Some(ref user) = global.user {
            nodes.push(p!["Logged in as ", &user.username]);
            nodes.push(a![
                attrs! { At::Href => "" },
                "Logout",
                ev(Ev::Click, |_| Msg::LogoutClicked)
            ]);
        } else {
            nodes.push(a![
                attrs! { At::Href => "" },
                "Login",
                ev(Ev::Click, |_| Msg::ToLoginPageClicked)
            ]);
        }
        nodes
    }

    fn view(&self, global: &GlobalData) -> Node<Msg> {
        let mut nodes: Vec<Node<Msg>> = Vec::new();
        nodes.push(h2!["Join lobby"]);
        nodes.push(form![
            div![
                label!["Username"],
                input![
                    attrs! {
                        At::Value => self.username,
                    },
                    input_ev(Ev::Input, Msg::UsernameChanged)
                ],
            ],
            div![
                label!["Lobby token"],
                input![
                    attrs! {
                        At::Value => self.lobby_token,
                    },
                    input_ev(Ev::Input, Msg::LobbyTokenChanged)
                ],
            ],
            button![
                "Join lobby",
                ev(Ev::Click, |event| {
                    event.prevent_default();
                    Msg::JoinLobbyClicked
                })
            ],
        ]);
        if global.user.is_some() {
            nodes.push(h2!["Create lobby"]);
            nodes.push(form![
                div![
                    label!["Lobby name"],
                    input![
                        attrs! {
                            At::Value => self.lobby_name,
                        },
                        input_ev(Ev::Input, Msg::LobbyNameChanged)
                    ],
                ],
                button![
                    "Create lobby",
                    ev(Ev::Click, |event| {
                        event.prevent_default();
                        Msg::CreateLobbyClicked
                    })
                ],
            ]);
        }
        return div![nodes];
    }
}

impl ConnectingModel {
    fn new(ws: WebSocket, username: String, lobby_token: String) -> Self {
        Self {
            ws: Some(ws),
            username,
            lobby_token,
        }
    }
}

impl<O: Orders<Msg>> Page<O> for ConnectingModel {
    fn update(&mut self, global: &mut GlobalData, msg: Msg, orders: &mut O) -> UpdateResult {
        match msg {
            Msg::WsError => {
                error!("Web socket connection errored out, disconnecting");
                return UpdateResult::Pop;
            }
            Msg::WsServerMessageReceived(msg) => match msg {
                ServerMessage::DuplicateName(_) => {
                    orders.send_msg(Msg::ErrorMessage("Name already taken".into()));
                    return UpdateResult::Pop;
                }
                ServerMessage::LobbyNotFound(_) => {
                    orders.send_msg(Msg::ErrorMessage("Lobby not found".into()));
                    return UpdateResult::Pop;
                }
                ServerMessage::SubmitUser(msg) => {
                    orders.send_msg(Msg::WsServerMessageReceived(ServerMessage::SubmitUser(msg)));
                    return UpdateResult::Replace(ModelPage::Connected(ConnectedModel::new(
                        self.ws.take().unwrap(),
                        self.username.clone(),
                        self.lobby_token.clone(),
                    )));
                }
                ServerMessage::SubmitLobby(msg) => {
                    orders.send_msg(Msg::WsServerMessageReceived(ServerMessage::SubmitLobby(
                        msg,
                    )));
                    return UpdateResult::Replace(ModelPage::Connected(ConnectedModel::new(
                        self.ws.take().unwrap(),
                        self.username.clone(),
                        self.lobby_token.clone(),
                    )));
                }
                ServerMessage::SendGames(msg) => {
                    orders.send_msg(Msg::WsServerMessageReceived(ServerMessage::SendGames(msg)));
                    return UpdateResult::Replace(ModelPage::Connected(ConnectedModel::new(
                        self.ws.take().unwrap(),
                        self.username.clone(),
                        self.lobby_token.clone(),
                    )));
                }
                ServerMessage::Error(msg) => {
                    error!(format!("Server error: {}, disconnecting", msg.message));
                    return UpdateResult::Pop;
                }
                _ => {}
            },
            m => crate::util::handle_other_msg(m, global, orders),
        }
        UpdateResult::Ok
    }
}

impl PageView for ConnectingModel {
    fn view(&self, _: &GlobalData) -> Node<Msg> {
        div![p!["Connecting"]]
    }
}

impl LoginModel {
    pub fn new() -> Self {
        Self {
            username: "".into(),
            password: "".into(),
            stay_logged_in: false,
        }
    }
}

impl<O: Orders<Msg>> Page<O> for LoginModel {
    fn update(&mut self, global: &mut GlobalData, msg: Msg, orders: &mut O) -> UpdateResult {
        match msg {
            Msg::UsernameChanged(v) => self.username = v,
            Msg::PasswordChanged(v) => self.password = v,
            Msg::StayLoggedInClicked => self.stay_logged_in = !self.stay_logged_in,
            Msg::LoginClicked => {
                let url = api::get_api_endpoint(api::API_LOGIN);
                let params = api::UserLogin {
                    username: self.username.clone(),
                    password: self.password.clone(),
                };
                let request = Request::new(url)
                    .method(Method::Post)
                    .json(&params)
                    .unwrap();
                orders.perform_cmd(crate::util::fetch_response(request, Msg::LoginFetched));
            }
            Msg::LoginFetched(FetchResult::Ok(response::TokenResponse::Ok(token))) => {
                let user = UserData {
                    token: token.token,
                    username: self.username.clone(),
                };
                global.set_user(user, self.stay_logged_in);
                return UpdateResult::Pop;
            }
            _ => {}
        }
        UpdateResult::Ok
    }
}

impl PageView for LoginModel {
    fn view(&self, _: &GlobalData) -> Node<Msg> {
        form![
            div![
                label!["Username"],
                input![
                    attrs! {
                        At::Value => self.username,
                    },
                    input_ev(Ev::Input, Msg::UsernameChanged)
                ],
            ],
            div![
                label!["Password"],
                input![
                    attrs! {
                        At::Value => self.password,
                        At::Type => "password",
                    },
                    input_ev(Ev::Input, Msg::PasswordChanged)
                ],
            ],
            div![
                attrs! {
                    At::Class => "form-checkbox",
                },
                input![
                    attrs! {
                        At::Type => "checkbox",
                        At::Checked => self.stay_logged_in.as_at_value(),
                    },
                    ev(Ev::Click, |_| Msg::StayLoggedInClicked)
                ],
                label!["Stay logged in"],
            ],
            button![
                "Login",
                ev(Ev::Click, |event| {
                    event.prevent_default();
                    Msg::LoginClicked
                })
            ],
        ]
    }
}
