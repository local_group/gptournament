mod api;
mod model;
mod util;

use model::{Model, Msg};
use seed::{prelude::*, *};

fn init(_: Url, orders: &mut impl Orders<Msg>) -> Model {
    Model::new(orders)
}

fn update(msg: Msg, model: &mut Model, orders: &mut impl Orders<Msg>) {
    model.update(msg, orders);
}

fn view(model: &Model) -> Vec<Node<Msg>> {
    model.view()
}

#[wasm_bindgen(start)]
pub fn start() {
    App::start("app", init, update, view);
}
