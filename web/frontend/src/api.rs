use serde::Serialize;

pub use web_common::types::{
    request::{Name, TokenName, UserLogin},
    response::Token,
};

pub const API_LOGIN: &str = "login";
pub const API_USER_INFO: &str = "user_info";
pub const API_CREATE_LOBBY: &str = "game/create_lobby";

pub fn get_ws_url(username: &str, lobby_token: &str) -> String {
    let location = seed::util::window().location();
    let host = location.host().unwrap();
    let protocol = if location.protocol().unwrap() == "https:" {
        "wss:"
    } else {
        "ws:"
    };
    let param = TokenName {
        token: lobby_token.into(),
        name: username.into(),
    };
    format!(
        "{}//{}/api/stream?{}",
        protocol,
        host,
        serde_urlencoded::to_string(param).unwrap()
    )
}

pub fn get_api_base_url() -> String {
    let location = seed::util::window().location();
    let host = location.host().unwrap();
    let protocol = location.protocol().unwrap();
    format!("{}//{}/api/", protocol, host)
}

pub fn get_api_endpoint(endpoint: &str) -> String {
    let location = seed::util::window().location();
    let host = location.host().unwrap();
    let protocol = location.protocol().unwrap();
    format!("{}//{}/api/{}", protocol, host, endpoint)
}

pub fn get_api_get_endpoint<P: Serialize>(endpoint: &str, params: P) -> String {
    let location = seed::util::window().location();
    let host = location.host().unwrap();
    let protocol = location.protocol().unwrap();
    format!(
        "{}//{}/api/{}?{}",
        protocol,
        host,
        endpoint,
        serde_urlencoded::to_string(params).unwrap()
    )
}
