use super::{GlobalData, Msg, Page, PageView, UpdateResult};
use crate::util::{IntoDragEvent, IntoKeyboardEvent};
use seed::{prelude::*, *};
use std::collections::HashMap;
use web_common::types::websocket::{self, ClientMessage, ServerMessage};

const SEND_TYPING_NOTIFICATION_TIMEOUT: u32 = 1000;
const RECV_TYPING_NOTIFICATION_TIMEOUT: u32 = 5000;

#[derive(Debug)]
pub struct ConnectedModel {
    ws: WebSocket,
    current_input_text: String,
    status_text: String,
    can_send_typing_notification: bool,
    typing_notification_handles: HashMap<String, CmdHandle>,
    gpt_generating: bool,
    username: String,
    lobby_token: String,
    lobby_name: String,
    lobby_users: Vec<String>,
    lobby_leader: String,
    is_leader: bool,
    game: Option<Game>,
}

#[derive(Debug)]
pub struct Game {
    players: Vec<String>,
    current_player: Option<String>,
    alive: Vec<String>,
    items: Vec<websocket::SendGamesMessageGameItem>,
}

impl Game {
    fn message_item_mut(
        &mut self,
        feed: u32,
    ) -> Option<&mut websocket::SendGamesMessageGameItemMessage> {
        self.items
            .iter_mut()
            .filter_map(|i| {
                if let websocket::SendGamesMessageGameItem::Message(msg) = i {
                    Some(msg)
                } else {
                    None
                }
            })
            .find(|msg| msg.response.feed == feed)
    }

    fn message_item_last_sequence_mut(
        &mut self,
        feed: u32,
    ) -> Option<&mut websocket::SendGamesMessageGameItemMessageResponseSequence> {
        self.message_item_mut(feed)
            .and_then(|msg| msg.response.sequence.last_mut())
    }
}

impl ConnectedModel {
    pub fn new(ws: WebSocket, username: String, lobby_token: String) -> Self {
        let mut this = Self {
            ws,
            current_input_text: "".into(),
            username,
            lobby_token,
            status_text: "".into(),
            can_send_typing_notification: true,
            typing_notification_handles: HashMap::new(),
            gpt_generating: false,
            lobby_name: "".into(),
            lobby_users: Vec::new(),
            lobby_leader: "".into(),
            is_leader: false,
            game: None,
        };
        this.update_status_text();
        this
    }

    fn is_current_player(&self) -> bool {
        self.game
            .as_ref()
            .and_then(|game| {
                game.current_player
                    .as_ref()
                    .map(|player| player == &self.username)
            })
            .unwrap_or(false)
    }

    fn update_status_text(&mut self) {
        if !self.current_input_text.contains(&self.username) {
            self.status_text = "Message must contain your own name".into();
        } else if !self.is_current_player() {
            self.status_text = "It's not your turn yet".into();
        } else if self.gpt_generating {
            self.status_text = "GPT is still generating text".into();
        } else {
            self.status_text = "".into();
        }
    }

    fn render_sidebar(&self) -> Vec<Node<Msg>> {
        let mut players = Vec::new();
        let empty = vec![];
        let game_players = self
            .game
            .as_ref()
            .map(|game| &game.players)
            .unwrap_or(&empty);
        for p in game_players {
            players.push(p);
        }
        for p in self.lobby_users.iter() {
            if !game_players.contains(p) {
                players.push(p);
            }
        }
        let mut has_separator = false;
        let mut sidebar_nodes = Vec::new();
        for user in players {
            if self
                .game
                .as_ref()
                .map(|game| !game.players.contains(user))
                .unwrap_or(false)
                && !has_separator
            {
                sidebar_nodes.push(div![attrs! { At::Class => "separator" }]);
                has_separator = true;
            }
            let mut user_node: Node<Msg> = div![];
            let mut class = String::from("player");
            if user == &self.lobby_leader {
                class.push_str(" player-leader");
            }
            if self.is_leader {
                class.push_str(" player-draggable");
                user_node.add_attr(At::Draggable.as_str(), "true");
            }
            if self
                .game
                .as_ref()
                .map(|game| Some(user) == game.current_player.as_ref())
                .unwrap_or(false)
            {
                class.push_str(" player-current");
                user_node.add_child(i![attrs! { At::Class => "bx bx-caret-right" }]);
            } else {
                user_node.add_child(i![]);
            }
            user_node.add_child(span![user]);
            if let Some(ref game) = self.game {
                if game.players.contains(user) {
                    if game.alive.contains(user) {
                        let user = user.clone();
                        user_node.add_child(i![
                            attrs! { At::Class => "bx bx-x" },
                            ev(Ev::Click, |_| Msg::KillClicked(user)),
                        ]);
                    } else {
                        class.push_str(" player-dead");
                        let user = user.clone();
                        user_node.add_child(i![
                            attrs! { At::Class => "bx bx-plus" },
                            ev(Ev::Click, |_| Msg::ReviveClicked(user)),
                        ]);
                    }
                }
            }

            user_node.add_attr(At::Class.as_str(), class);

            let user_ev = user.clone();
            user_node.add_event_handler(EventHandler::new(Ev::DragStart, move |event| {
                let drag_event = event.into_drag_event();
                let _ = drag_event
                    .data_transfer()
                    .unwrap()
                    .set_data("text/plain", &user_ev);
                None
            }));
            user_node.add_event_handler(EventHandler::new(Ev::DragEnter, |event| {
                let drag_event = event.into_drag_event();
                drag_event.prevent_default();
                drag_event.stop_propagation();
                let element = drag_event
                    .current_target()
                    .expect("cannot get event target")
                    .dyn_into::<web_sys::Element>()
                    .expect("cannot cast given event target into Element");
                let offset_y = drag_event.offset_y();
                let height = element.client_height();
                let after = offset_y >= height / 2;
                let _ = if after {
                    element.class_list().add_1("drop-after")
                } else {
                    element.class_list().add_1("drop-before")
                };
                None
            }));
            user_node.add_event_handler(EventHandler::new(Ev::DragOver, |event| {
                let drag_event = event.into_drag_event();
                drag_event.prevent_default();
                drag_event.stop_propagation();
                let element = drag_event
                    .current_target()
                    .expect("cannot get event target")
                    .dyn_into::<web_sys::Element>()
                    .expect("cannot cast given event target into Element");
                let offset_y = drag_event.offset_y();
                let height = element.client_height();
                let after = offset_y >= height / 2;
                let _ = if after {
                    let _ = element.class_list().remove_1("drop-before");
                    element.class_list().add_1("drop-after")
                } else {
                    let _ = element.class_list().remove_1("drop-after");
                    element.class_list().add_1("drop-before")
                };
                None
            }));
            user_node.add_event_handler(EventHandler::new(Ev::DragLeave, |event| {
                let drag_event = event.into_drag_event();
                drag_event.prevent_default();
                drag_event.stop_propagation();
                let element = drag_event
                    .current_target()
                    .expect("cannot get event target")
                    .dyn_into::<web_sys::Element>()
                    .expect("cannot cast given event target into Element");
                let _ = element.class_list().remove_2("drop-after", "drop-before");
                None
            }));
            let user_ev = user.clone();
            user_node.add_event_handler(EventHandler::new(Ev::Drop, move |event| {
                let drag_event = event.into_drag_event();
                drag_event.prevent_default();
                drag_event.stop_propagation();
                if let Ok(user) = drag_event.data_transfer().unwrap().get_data("text/plain") {
                    let element = drag_event
                        .current_target()
                        .expect("cannot get event target")
                        .dyn_into::<web_sys::Element>()
                        .expect("cannot cast given event target into Element");
                    let _ = element.class_list().remove_2("drop-after", "drop-before");
                    if user != user_ev {
                        let offset_y = drag_event.offset_y();
                        let height = element.client_height();
                        let after = offset_y >= height / 2;
                        return Some(Msg::ReorderPlayer {
                            source_player: user,
                            target_player: user_ev.clone(),
                            after,
                        });
                    }
                }
                None
            }));

            sidebar_nodes.push(user_node);
        }
        sidebar_nodes
    }

    fn render_main(&self) -> Vec<Node<Msg>> {
        let top_panel = div![
            attrs! { At::Class => "message-history" },
            self.render_chat_history(),
        ];
        let bottom_panel = if self.game.is_some() {
            div![
                attrs! { At::Class => "bottom-panel" },
                self.render_message_input()
            ]
        } else {
            div![
                attrs! { At::Class => "bottom-panel" },
                self.render_game_start()
            ]
        };
        vec![top_panel, bottom_panel]
    }

    fn render_chat_history(&self) -> Vec<Node<Msg>> {
        let game = if let Some(game) = self.game.as_ref() {
            game
        } else {
            return vec![empty![]];
        };
        let mut nodes = Vec::new();
        for item in &game.items {
            match item {
                websocket::SendGamesMessageGameItem::Message(msg) => {
                    nodes.push(div![
                        attrs! { At::Class => "message-row" },
                        div![
                            attrs! { At::Class => "message message-left" },
                            div![
                                attrs! { At::Class => "message-title" },
                                span![
                                    attrs! { At::Class => "message-title-name" },
                                    &msg.sender.user
                                ],
                                span![
                                    attrs! { At::Class => "message-title-date" },
                                    crate::util::render_date_time(&msg.sender.time)
                                ],
                            ],
                            div![
                                attrs! { At::Class => "message-body" },
                                div![self.render_message(&msg.sender.message)],
                            ],
                        ]
                    ]);
                    for response in &msg.response.sequence {
                        nodes.push(div![
                            attrs! { At::Class => "message-row" },
                            div![
                                attrs! { At::Class => "message message-right" },
                                div![
                                    attrs! { At::Class => "message-title" },
                                    span![attrs! { At::Class => "message-title-name" }, "GPT"],
                                    span![
                                        attrs! { At::Class => "message-title-date" },
                                        crate::util::render_date_time(&response.time)
                                    ],
                                ],
                                div![
                                    attrs! { At::Class => "message-body" },
                                    div![self.render_message(&response.message)],
                                ],
                            ]
                        ]);
                    }
                }
                websocket::SendGamesMessageGameItem::Revive(msg) => {
                    nodes.push(div![
                        attrs! { At::Class => "message-row" },
                        div![
                            attrs! { At::Class => "message-notice" },
                            div![span![
                                "Player ",
                                span![
                                    attrs! { At::Class => "message-player-highlight" },
                                    &msg.user
                                ],
                                " was revived.",
                            ]],
                        ],
                    ]);
                }
                websocket::SendGamesMessageGameItem::Kill(msg) => {
                    nodes.push(div![
                        attrs! { At::Class => "message-row" },
                        div![
                            attrs! { At::Class => "message-notice" },
                            div![span![
                                "Player ",
                                span![
                                    attrs! { At::Class => "message-player-highlight" },
                                    &msg.user
                                ],
                                " was killed.",
                            ]],
                        ],
                    ]);
                }
            }
        }
        nodes
    }

    fn render_message(&self, msg: &str) -> Vec<Node<Msg>> {
        use seed::virtual_dom::Text;

        let game_players = if let Some(p) = self.game.as_ref().map(|game| &game.players) {
            p
        } else {
            return vec![Node::new_text(msg.to_string())];
        };

        let mut nodes = vec![Node::new_text(msg.to_string())];
        for p in game_players {
            nodes = nodes
                .into_iter()
                .flat_map(|n| match n {
                    Node::Text(text) => find_single(text, p),
                    n => vec![n],
                })
                .collect()
        }
        return nodes;

        fn find_single(text: Text, player: &str) -> Vec<Node<Msg>> {
            let mut text = text.text.as_ref();
            let mut nodes = Vec::new();
            while let Some(position) = text.find(player) {
                if position > 0 {
                    nodes.push(Node::new_text(text[0..position].to_string()));
                }
                nodes.push(span![
                    attrs! { At::Class => "message-player-highlight" },
                    player
                ]);
                text = &text[(position + player.len())..];
            }
            if !text.is_empty() {
                nodes.push(Node::new_text(text.to_string()));
            }
            nodes
        }
    }

    fn render_message_input(&self) -> Vec<Node<Msg>> {
        let len = self.typing_notification_handles.len();
        let typing_text = match len {
            len if len >= 4 => "Several people are typing...".into(),
            0 => "".into(),
            len => {
                let mut text = String::new();
                for (i, player) in self.typing_notification_handles.keys().enumerate() {
                    text.push_str(player);
                    if i + 2 < len {
                        text.push_str(", ");
                    } else if i + 1 < len {
                        text.push_str(" and ");
                    }
                }
                if len == 1 {
                    text.push_str(" is typing...");
                } else {
                    text.push_str(" are typing...");
                }
                text
            }
        };
        vec![
            div![span![
                attrs! { At::Class => "status-line error" },
                &self.status_text
            ]],
            div![
                attrs! { At::Class => "input-box" },
                textarea![
                    attrs! { At::Placeholder => "Enter your message" },
                    &self.current_input_text,
                    input_ev(Ev::Input, Msg::MessageTextChanged),
                    ev(Ev::KeyDown, |event| {
                        let event = event.into_keyboard_event();
                        if event.code() == "Enter" || event.key_code() == 13 {
                            // enter pressed
                            event.prevent_default();
                            Some(Msg::SubmitMessageClicked)
                        } else {
                            None
                        }
                    }),
                ],
                div![
                    attrs! { At::Class => "input-submit" },
                    button![
                        i![attrs! { At::Class => "bx bx-chevron-right" }],
                        ev(Ev::Click, |event| {
                            event.prevent_default();
                            Msg::SubmitMessageClicked
                        })
                    ]
                ],
            ],
            div![span![attrs! { At::Class => "status-line" }, typing_text]],
        ]
    }

    fn render_game_start(&self) -> Vec<Node<Msg>> {
        vec![div![
            attrs! { At::Class => "start-game" },
            if self.is_leader {
                button![
                    "Start game",
                    i![attrs! { At::Class => "bx bx-chevron-right" }],
                    ev(Ev::Click, |event| {
                        event.prevent_default();
                        Msg::StartGameClicked
                    })
                ]
            } else {
                span![format!(
                    "Please wait for the leader ({}) to start the game",
                    self.lobby_leader
                )]
            }
        ]]
    }
}

impl<O: Orders<Msg>> Page<O> for ConnectedModel {
    fn update(&mut self, global: &mut GlobalData, msg: Msg, orders: &mut O) -> UpdateResult {
        match msg {
            Msg::WsError => {
                log!("Web socket connection errored out, disconnecting");
                return UpdateResult::Pop;
            }
            Msg::MessageTextChanged(v) => {
                self.current_input_text = v;
                if self.can_send_typing_notification {
                    let _ = self.ws.send_json(&ClientMessage::NotifyTyping(
                        websocket::NotifyTypingMessage {
                            user: self.username.clone(),
                            message: "".into(),
                        },
                    ));
                    self.can_send_typing_notification = false;
                    orders.perform_cmd(cmds::timeout(SEND_TYPING_NOTIFICATION_TIMEOUT, || {
                        Msg::SendTypingNotificationTimeout
                    }));
                }
                self.update_status_text();
            }
            Msg::SendTypingNotificationTimeout => {
                self.can_send_typing_notification = true;
            }
            Msg::ReceivedTypingNotificationTimeout(user) => {
                self.typing_notification_handles.remove(&user);
            }
            Msg::WsServerMessageReceived(msg) => match msg {
                ServerMessage::SubmitUser(msg) => {
                    self.lobby_users = msg.user;
                    self.lobby_leader = msg.leader;
                    self.is_leader = self.username == self.lobby_leader;
                }
                ServerMessage::SubmitLobby(msg) => {
                    self.lobby_name = msg.lobby_name;
                }
                ServerMessage::SendGames(msg) => {
                    self.game = Some(Game {
                        players: msg.game.user,
                        alive: Vec::new(),
                        current_player: None,
                        items: msg.game.items,
                    });
                }
                ServerMessage::NotifyTyping(msg) => {
                    let user = msg.user.clone();
                    let handle = orders.perform_cmd_with_handle(cmds::timeout(
                        RECV_TYPING_NOTIFICATION_TIMEOUT,
                        || Msg::ReceivedTypingNotificationTimeout(user),
                    ));
                    self.typing_notification_handles.insert(msg.user, handle);
                }
                ServerMessage::GameStarted(msg) => {
                    self.game = Some(Game {
                        players: msg.user,
                        alive: Vec::new(),
                        current_player: None,
                        items: Vec::new(),
                    });
                }
                ServerMessage::UpdatePlayer(msg) => {
                    if let Some(ref mut game) = self.game {
                        let killed_players = game
                            .alive
                            .iter()
                            .filter(|p| !msg.alive.contains(p))
                            .cloned()
                            .collect::<Vec<_>>();
                        let revived_players = msg
                            .alive
                            .iter()
                            .filter(|p| !game.alive.contains(p))
                            .cloned()
                            .collect::<Vec<_>>();
                        game.current_player = msg.current;
                        game.alive = msg.alive;
                        if matches!(
                            msg.update,
                            Some(websocket::UpdatePlayerMessageUpdate {
                                update_type: websocket::UpdatePlayerMessageUpdateType::Kill,
                                ..
                            })
                        ) {
                            for player in killed_players {
                                game.items.push(websocket::SendGamesMessageGameItem::Kill(
                                    websocket::SendGamesMessageGameItemKill { user: player },
                                ))
                            }
                        }
                        if matches!(
                            msg.update,
                            Some(websocket::UpdatePlayerMessageUpdate {
                                update_type: websocket::UpdatePlayerMessageUpdateType::Revive,
                                ..
                            })
                        ) {
                            for player in revived_players {
                                game.items.push(websocket::SendGamesMessageGameItem::Revive(
                                    websocket::SendGamesMessageGameItemRevive { user: player },
                                ))
                            }
                        }
                        orders.after_next_render(|_| Msg::ScrollMessagesToBottom);
                        self.update_status_text();
                    } else {
                        orders.send_msg(Msg::WsServerMessageReceived(ServerMessage::UpdatePlayer(
                            msg,
                        )));
                    }
                }
                ServerMessage::GameFinished(_) => {
                    self.game.take();
                }
                ServerMessage::InitResponse(msg) => {
                    if let Some(ref mut game) = self.game {
                        if let Some(user_submission) = msg.user_submission {
                            game.items
                                .push(websocket::SendGamesMessageGameItem::Message(
                                    websocket::SendGamesMessageGameItemMessage {
                                        sender: websocket::SendGamesMessageGameItemMessageSender {
                                            user: user_submission.user,
                                            message: user_submission.message,
                                            time: user_submission.time,
                                        },
                                        response: websocket::SendGamesMessageGameItemMessageResponse {
                                            feed: msg.feed,
                                            continue_until: msg.continue_until,
                                            sequence: vec![websocket::SendGamesMessageGameItemMessageResponseSequence {
                                                message: "".into(),
                                                time: crate::util::get_date_time(),
                                            }]
                                        },
                                    },
                                ));
                            orders.after_next_render(|_| Msg::ScrollMessagesToBottom);
                        } else if let Some(msg) = game.message_item_mut(msg.feed) {
                            msg.response.sequence.push(
                                websocket::SendGamesMessageGameItemMessageResponseSequence {
                                    message: "".into(),
                                    time: crate::util::get_date_time(),
                                },
                            );
                        }
                    }
                    self.gpt_generating = true;
                }
                ServerMessage::ResponseToken(msg) => {
                    if let Some(ref mut game) = self.game {
                        if let Some(seq) = game.message_item_last_sequence_mut(msg.feed) {
                            seq.message.push_str(&msg.text);
                            seq.time = crate::util::get_date_time();
                        }
                    }
                    orders.after_next_render(|_| Msg::ScrollMessagesToBottom);
                }
                ServerMessage::FullResponse(msg) => {
                    if let Some(ref mut game) = self.game {
                        if let Some(seq) = game.message_item_last_sequence_mut(msg.feed) {
                            seq.message = msg.text;
                            seq.time = msg.time;
                        }
                    }
                    orders.after_next_render(|_| Msg::ScrollMessagesToBottom);
                    self.gpt_generating = false;
                }
                ServerMessage::Error(msg) => {
                    log!("Server error: {}", msg.message);
                }
                _ => {}
            },
            Msg::ReorderPlayer {
                source_player,
                target_player,
                after,
            } => {
                let dst_idx = self
                    .lobby_users
                    .iter()
                    .enumerate()
                    .find(|(_, p)| *p == &target_player)
                    .map(|(i, _)| i);
                if let Some(mut dst_idx) = dst_idx {
                    if after {
                        dst_idx += 1;
                    }
                    let _ = self.ws.send_json(&ClientMessage::ReorderUser(
                        websocket::ReorderUserMessage {
                            user: source_player,
                            index: dst_idx as i32,
                            relative: false,
                        },
                    ));
                }
            }
            Msg::SubmitMessageClicked => {
                if !self.is_current_player()
                    || !self.current_input_text.contains(&self.username)
                    || self.gpt_generating
                {
                    return UpdateResult::Ok;
                }
                let _ = self.ws.send_json(&ClientMessage::SubmitMessage(
                    websocket::SubmitMessageMessage {
                        message: self.current_input_text.clone(),
                        user: self.username.clone(),
                        token: None,
                    },
                ));
                let element = window()
                    .document()
                    .and_then(|doc| doc.query_selector(".input-box textarea").ok().flatten())
                    .and_then(|element| element.dyn_into::<web_sys::HtmlTextAreaElement>().ok());
                if let Some(element) = element {
                    element.set_value("");
                    self.current_input_text.clear();
                }
            }
            Msg::StartGameClicked => {
                let _ = self
                    .ws
                    .send_json(&ClientMessage::CreateGame(websocket::CreateGameMessage {}));
            }
            Msg::KillClicked(player) => {
                let _ =
                    self.ws
                        .send_json(&ClientMessage::KillPlayer(websocket::KillPlayerMessage {
                            user: player,
                        }));
            }
            Msg::ReviveClicked(player) => {
                let _ = self.ws.send_json(&ClientMessage::RevivePlayer(
                    websocket::RevivePlayerMessage { user: player },
                ));
            }
            Msg::ScrollMessagesToBottom => {
                let element = window()
                    .document()
                    .and_then(|doc| doc.query_selector(".message-history").ok().flatten())
                    .and_then(|element| element.last_element_child());
                if let Some(element) = element {
                    element.scroll_into_view_with_bool(false);
                }
            }
            m => crate::util::handle_other_msg(m, global, orders),
        }
        UpdateResult::Ok
    }
}

impl PageView for ConnectedModel {
    fn view_nav_left(&self, _: &GlobalData) -> Vec<Node<Msg>> {
        vec![p![format!(
            "Lobby: {} ({})",
            self.lobby_name, self.lobby_token
        )]]
    }

    fn view_nav_right(&self, _: &GlobalData) -> Vec<Node<Msg>> {
        vec![p!["Connected as ", &self.username]]
    }

    fn view(&self, _: &GlobalData) -> Node<Msg> {
        let sidebar = self.render_sidebar();
        let main = self.render_main();
        div![
            attrs! { At::Class => "gpt-game" },
            div![attrs! { At::Class => "sidebar" }, sidebar],
            div![attrs! { At::Class => "main" }, main],
        ]
    }
}
