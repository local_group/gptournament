use crate::types::StateId;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
#[serde(rename_all = "snake_case")]
pub struct UserLogin {
    pub username: String,
    pub password: String,
}

#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
#[serde(rename_all = "snake_case")]
pub struct Token {
    pub token: String,
}

#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
#[serde(rename_all = "snake_case")]
pub struct Id {
    pub id: StateId,
}

#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
#[serde(rename_all = "snake_case")]
pub struct IdLine {
    pub id: StateId,
    pub line: String,
}

#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
#[serde(rename_all = "snake_case")]
pub struct Line {
    pub line: String,
}

#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
#[serde(rename_all = "snake_case")]
pub struct TokenName {
    pub token: String,
    pub name: String,
}

#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
#[serde(rename_all = "snake_case")]
pub struct Name {
    pub name: String,
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_de_user_login() {
        assert_eq!(
            serde_urlencoded::from_str::<UserLogin>(
                "username=testuser&password=verysecurepassword"
            )
            .unwrap(),
            UserLogin {
                username: "testuser".into(),
                password: "verysecurepassword".into()
            }
        );
    }

    #[test]
    fn test_de_token() {
        assert_eq!(
            serde_urlencoded::from_str::<Token>("token=token_value").unwrap(),
            Token {
                token: "token_value".into()
            }
        );
    }

    #[test]
    fn test_de_id() {
        assert_eq!(
            serde_urlencoded::from_str::<Id>("id=0").unwrap(),
            Id { id: 0 }
        );
    }

    #[test]
    fn test_de_id_line() {
        assert_eq!(
            serde_urlencoded::from_str::<IdLine>("id=0&line=Hello,%20world!%0a").unwrap(),
            IdLine {
                id: 0,
                line: "Hello, world!\n".into()
            }
        );
    }

    #[test]
    fn test_de_line() {
        assert_eq!(
            serde_urlencoded::from_str::<Line>("line=Hello,%20world!%0a").unwrap(),
            Line {
                line: "Hello, world!\n".into()
            }
        );
    }

    #[test]
    fn test_de_token_name() {
        assert_eq!(
            serde_urlencoded::from_str::<TokenName>("token=token_value&name=player_name").unwrap(),
            TokenName {
                token: "token_value".into(),
                name: "player_name".into(),
            }
        );
    }

    #[test]
    fn test_de_name() {
        assert_eq!(
            serde_urlencoded::from_str::<Name>("name=name_value").unwrap(),
            Name {
                name: "name_value".into(),
            }
        );
    }
}
