use serde::{Deserialize, Serialize};

pub type Timestamp = chrono::DateTime<chrono::Local>;

#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
#[serde(rename_all = "kebab-case", tag = "$type")]
pub enum ServerMessage {
    LobbyNotFound(LobbyNotFoundMessage),
    DuplicateName(DuplicateNameMessage),
    SubmitLobby(SubmitLobbyMessage),
    SendGames(SendGamesMessage),
    GameStarted(GameStartedMessage),
    SubmitUser(SubmitUserMessage),
    NotifyTyping(NotifyTypingMessage),
    InitResponse(InitResponseMessage),
    ResponseToken(ResponseTokenMessage),
    FullResponse(FullResponseMessage),
    UpdatePlayer(UpdatePlayerMessage),
    GameFinished(GameFinishedMessage),
    Error(ErrorMessage),
}

#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
#[serde(rename_all = "kebab-case", tag = "$type")]
pub enum ClientMessage {
    CreateGame(CreateGameMessage),
    PromoteLeader(PromoteLeaderMessage),
    KickUser(KickUserMessage),
    ReorderUser(ReorderUserMessage),
    SubmitMessage(SubmitMessageMessage),
    NotifyTyping(NotifyTypingMessage),
    ContinueResponse(ContinueResponseMessage),
    KillPlayer(KillPlayerMessage),
    RevivePlayer(RevivePlayerMessage),
    SkipPlayer(SkipPlayerMessage),
    GameFinished(GameFinishedMessage),
}

#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
#[serde(rename_all = "kebab-case")]
pub struct LobbyNotFoundMessage {}

#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
#[serde(rename_all = "kebab-case")]
pub struct DuplicateNameMessage {}

#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
#[serde(rename_all = "kebab-case")]
pub struct SubmitLobbyMessage {
    pub lobby_name: String,
}

#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
#[serde(rename_all = "kebab-case")]
pub struct SendGamesMessage {
    pub game: SendGamesMessageGame,
}

#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
#[serde(rename_all = "kebab-case")]
pub struct SendGamesMessageGame {
    pub user: Vec<String>,
    pub items: Vec<SendGamesMessageGameItem>,
}

#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
#[serde(rename_all = "kebab-case", tag = "$type")]
pub enum SendGamesMessageGameItem {
    Message(SendGamesMessageGameItemMessage),
    Kill(SendGamesMessageGameItemKill),
    Revive(SendGamesMessageGameItemRevive),
}

#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
#[serde(rename_all = "kebab-case")]
pub struct SendGamesMessageGameItemMessage {
    pub sender: SendGamesMessageGameItemMessageSender,
    pub response: SendGamesMessageGameItemMessageResponse,
}

#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
#[serde(rename_all = "kebab-case")]
pub struct SendGamesMessageGameItemMessageSender {
    pub time: Timestamp,
    pub user: String,
    pub message: String,
}

#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
#[serde(rename_all = "kebab-case")]
pub struct SendGamesMessageGameItemMessageResponse {
    pub feed: u32,
    pub continue_until: Timestamp,
    pub sequence: Vec<SendGamesMessageGameItemMessageResponseSequence>,
}

#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
#[serde(rename_all = "kebab-case")]
pub struct SendGamesMessageGameItemMessageResponseSequence {
    pub time: Timestamp,
    pub message: String,
}

#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
#[serde(rename_all = "kebab-case")]
pub struct SendGamesMessageGameItemKill {
    pub user: String,
}

#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
#[serde(rename_all = "kebab-case")]
pub struct SendGamesMessageGameItemRevive {
    pub user: String,
}

#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
#[serde(rename_all = "kebab-case")]
pub struct CreateGameMessage {}

#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
#[serde(rename_all = "kebab-case")]
pub struct GameStartedMessage {
    pub user: Vec<String>,
}

#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
#[serde(rename_all = "kebab-case")]
pub struct PromoteLeaderMessage {
    pub user: String,
}

#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
#[serde(rename_all = "kebab-case")]
pub struct SubmitUserMessage {
    pub user: Vec<String>,
    pub leader: String,
}

#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
#[serde(rename_all = "kebab-case")]
pub struct KickUserMessage {
    pub user: String,
}

#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
#[serde(rename_all = "kebab-case")]
pub struct ReorderUserMessage {
    pub user: String,
    pub relative: bool,
    pub index: i32,
}

#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
#[serde(rename_all = "kebab-case")]
pub struct SubmitMessageMessage {
    pub user: String,
    pub message: String,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub token: Option<String>,
}

#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
#[serde(rename_all = "kebab-case")]
pub struct NotifyTypingMessage {
    pub user: String,
    pub message: String,
}

#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
#[serde(rename_all = "kebab-case")]
pub struct ContinueResponseMessage {
    pub feed: u32,
}

#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
#[serde(rename_all = "kebab-case")]
pub struct InitResponseMessage {
    pub feed: u32,
    pub sequence: u32,
    pub continue_until: Timestamp,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub user_submission: Option<InitResponseMessageUserSubmission>,
}

#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
#[serde(rename_all = "kebab-case")]
pub struct InitResponseMessageUserSubmission {
    pub time: Timestamp,
    pub user: String,
    pub message: String,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub token: Option<String>,
}

#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
#[serde(rename_all = "kebab-case")]
pub struct ResponseTokenMessage {
    pub feed: u32,
    pub sequence: u32,
    pub index: u32,
    pub text: String,
}

#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
#[serde(rename_all = "kebab-case")]
pub struct FullResponseMessage {
    pub feed: u32,
    pub sequence: u32,
    pub text: String,
    pub time: Timestamp,
}

#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
#[serde(rename_all = "kebab-case")]
pub struct KillPlayerMessage {
    pub user: String,
}

#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
#[serde(rename_all = "kebab-case")]
pub struct RevivePlayerMessage {
    pub user: String,
}

#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
#[serde(rename_all = "kebab-case")]
pub struct SkipPlayerMessage {
    pub user: String,
}

#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
#[serde(rename_all = "kebab-case")]
pub struct UpdatePlayerMessage {
    pub alive: Vec<String>,
    pub current: Option<String>,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub update: Option<UpdatePlayerMessageUpdate>,
}

#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
#[serde(rename_all = "kebab-case")]
pub struct UpdatePlayerMessageUpdate {
    #[serde(rename = "type")]
    pub update_type: UpdatePlayerMessageUpdateType,
    pub user: String,
}

#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
#[serde(rename_all = "kebab-case")]
pub enum UpdatePlayerMessageUpdateType {
    Kill,
    Revive,
}

#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
#[serde(rename_all = "kebab-case")]
pub struct GameFinishedMessage {}

#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
#[serde(rename_all = "kebab-case")]
pub struct ErrorMessage {
    pub message: String,
}

impl ServerMessage {
    pub fn error<S: Into<String>>(msg: S) -> Self {
        Self::Error(ErrorMessage {
            message: msg.into(),
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_de_client_messages() {
        assert_eq!(
            serde_json::from_str::<ClientMessage>(
                r#"{"$type":"kill-player", "user": "test_user"}"#
            )
            .unwrap(),
            ClientMessage::KillPlayer(KillPlayerMessage {
                user: "test_user".into(),
            })
        );
        assert_eq!(
            serde_json::from_str::<ClientMessage>(
                r#"{"$type":"submit-message", "user": "test_user", "message": "message_content"}"#
            )
            .unwrap(),
            ClientMessage::SubmitMessage(SubmitMessageMessage {
                user: "test_user".into(),
                message: "message_content".into(),
                token: None,
            })
        );
        assert_eq!(
            serde_json::from_str::<ClientMessage>(
                r#"{"$type":"submit-message", "user": "test_user", "message": "message_content","token":"t"}"#
            )
                .unwrap(),
            ClientMessage::SubmitMessage(SubmitMessageMessage {
                user: "test_user".into(),
                message: "message_content".into(),
                token: Some("t".into()),
            })
        );
    }

    #[test]
    fn test_ser_server_messages() {
        assert_eq!(
            serde_json::to_string(&ServerMessage::UpdatePlayer(UpdatePlayerMessage {
                alive: vec!["Alice".into(), "Bob".into()],
                current: Some("Alice".into()),
                update: Some(UpdatePlayerMessageUpdate {
                    update_type: UpdatePlayerMessageUpdateType::Kill,
                    user: "Charlie".into(),
                })
            }))
            .unwrap(),
            r#"{"$type":"update-player","alive":["Alice","Bob"],"current":"Alice","update":{"type":"kill","user":"Charlie"}}"#
        );
        assert_eq!(
            serde_json::to_string(&ServerMessage::UpdatePlayer(UpdatePlayerMessage {
                alive: vec!["Alice".into(), "Bob".into()],
                current: Some("Alice".into()),
                update: None
            }))
            .unwrap(),
            r#"{"$type":"update-player","alive":["Alice","Bob"],"current":"Alice"}"#
        );
    }
}
