use crate::types::StateId;
#[cfg(feature = "backend")]
use actix_web::{HttpRequest, HttpResponse, Responder};
#[cfg(feature = "backend")]
use futures_util::future::{ready, Ready};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
#[serde(tag = "status", rename_all = "snake_case")]
pub enum StatusResponse {
    Ok,
    Error { message: String },
}

#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
#[serde(tag = "status", rename_all = "snake_case")]
pub enum Response<T> {
    Ok(T),
    Error { message: String },
}

pub type TokenResponse = Response<Token>;
pub type IdResponse = Response<Id>;
pub type UserListResponse = Response<UserList>;
pub type UserTypeResponse = Response<UserType>;

#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
pub struct Token {
    pub token: String,
}

impl From<String> for Token {
    fn from(token: String) -> Self {
        Self { token }
    }
}

#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
pub struct Id {
    pub id: StateId,
}

impl From<StateId> for Id {
    fn from(id: StateId) -> Self {
        Self { id }
    }
}

#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
pub struct UserList {
    pub users: Vec<User>,
}

impl From<Vec<User>> for UserList {
    fn from(users: Vec<User>) -> Self {
        Self { users }
    }
}

#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
pub struct User {
    pub username: String,
}

#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
#[serde(tag = "user_type", rename_all = "snake_case")]
pub enum UserType {
    NotLoggedIn,
    ApiKey,
    User(UserInfo),
}

#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
pub struct UserInfo {
    pub username: String,
    pub is_admin: bool,
}

impl StatusResponse {
    pub fn error<S: Into<String>>(message: S) -> Self {
        Self::Error {
            message: message.into(),
        }
    }
}

impl<T> Response<T> {
    pub fn error<S: Into<String>>(message: S) -> Self {
        Self::Error {
            message: message.into(),
        }
    }

    pub fn ok<U: Into<T>>(data: U) -> Self {
        Self::Ok(data.into())
    }
}

#[cfg(feature = "backend")]
impl Responder for StatusResponse {
    type Error = ();
    type Future = Ready<Result<HttpResponse, <Self as Responder>::Error>>;

    fn respond_to(self, _req: &HttpRequest) -> Self::Future {
        ready(Ok(HttpResponse::Ok().json(self)))
    }
}

#[cfg(feature = "backend")]
impl<T: Serialize> Responder for Response<T> {
    type Error = ();
    type Future = Ready<Result<HttpResponse, <Self as Responder>::Error>>;

    fn respond_to(self, _req: &HttpRequest) -> Self::Future {
        ready(Ok(HttpResponse::Ok().json(self)))
    }
}

impl<T> From<T> for Response<T> {
    fn from(data: T) -> Self {
        Self::Ok(data)
    }
}

impl<T, E: std::fmt::Display> From<Result<T, E>> for Response<T> {
    fn from(data: Result<T, E>) -> Self {
        match data {
            Ok(data) => Self::Ok(data),
            Err(e) => Self::error(format!("{}", e)),
        }
    }
}

impl From<Response<()>> for StatusResponse {
    fn from(resp: Response<()>) -> Self {
        match resp {
            Response::Ok(_) => Self::Ok,
            Response::Error { message: m } => Self::Error { message: m },
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_ser_status_response_error() {
        assert_eq!(
            serde_json::to_string(&StatusResponse::error("error message")).unwrap(),
            "{\"status\":\"error\",\"message\":\"error message\"}"
        );
    }

    #[test]
    fn test_ser_status_response_ok() {
        assert_eq!(
            serde_json::to_string(&StatusResponse::Ok).unwrap(),
            "{\"status\":\"ok\"}"
        );
    }

    #[test]
    fn test_ser_response_error() {
        assert_eq!(
            serde_json::to_string(&Response::<()>::error("error message")).unwrap(),
            "{\"status\":\"error\",\"message\":\"error message\"}"
        );
    }

    #[test]
    fn test_ser_response_token() {
        assert_eq!(
            serde_json::to_string(&TokenResponse::ok("token_value".to_string())).unwrap(),
            "{\"status\":\"ok\",\"token\":\"token_value\"}"
        );
    }

    #[test]
    fn test_ser_response_id() {
        assert_eq!(
            serde_json::to_string(&IdResponse::ok(1)).unwrap(),
            "{\"status\":\"ok\",\"id\":1}"
        );
    }

    #[test]
    fn test_ser_response_user_list() {
        assert_eq!(
            serde_json::to_string(&UserListResponse::ok(vec![
                User {
                    username: "user0".into()
                },
                User {
                    username: "user1".into()
                }
            ]))
            .unwrap(),
            "{\"status\":\"ok\",\"users\":[{\"username\":\"user0\"},{\"username\":\"user1\"}]}"
        );
    }
}
