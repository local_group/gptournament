pub mod request;
pub mod response;
pub mod websocket;

pub type StateId = u32;
