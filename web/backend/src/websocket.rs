use crate::{
    config::Config,
    resources::Model,
    state::{
        game::{Game, GameFeed, GameFeedSequence, UserSubmission},
        GameLobby, Tokens,
    },
};
use actix::{Actor, ActorContext, Addr, AsyncContext, Handler, Running, StreamHandler};
use actix_web::{web, HttpRequest};
use actix_web_actors::ws::{self, Message, ProtocolError};
use futures_channel::mpsc::{self, Receiver, Sender};
use futures_executor::block_on;
use futures_util::{lock::Mutex, stream::BoxStream, StreamExt};
use gpt::{
    game::state::{GameData, GameItem, GptResponse},
    GameError,
};
use std::{ops::Add, sync::Arc};
use web_common::types::websocket::{self, ClientMessage, ServerMessage};

lazy_static::lazy_static! {
    static ref CONTINUE_DURATION: chrono::Duration = chrono::Duration::minutes(9);
}

pub enum ClientMessageResult {
    Ok,
    AddStream(BoxStream<'static, GeneratorTokenMessage>),
}

#[derive(Debug, Clone)]
pub struct ActixServerMessage(ServerMessage);

pub struct GeneratorTokenMessage {
    token: String,
}

#[derive(Debug)]
pub enum ConnectionStreamMessage {
    Text(String),
    Close,
}

#[derive(Debug)]
pub enum InternalMessage {
    SetLeader,
}

#[derive(Clone)]
pub struct GameLobbyConnection {
    pub(crate) addr: Addr<Connection>,
    identifier: String,
    player_name: String,
}

pub struct Connection {
    config: Config,
    tokens: Tokens,
    model: Model,
    state: ConnectionState,
    pub server_tx: Sender<ServerMessage>,
    server_rx: Option<Receiver<ServerMessage>>,

    lobby_token: Option<String>,
    lobby: Option<Arc<Mutex<GameLobby>>>,
    player_name: String,
    is_leader: bool,
    identifier: String,
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum ConnectionState {
    Opening,
    Open,
    ServerClosed,
    ClientClosed,
    Closed,
}

impl Connection {
    pub fn new(req: &HttpRequest, lobby_token: String, player_name: String) -> Option<Self> {
        log::debug!("Creating new web socket connection");
        let config = req.app_data::<web::Data<Config>>()?.get_ref().clone();
        let tokens = req.app_data::<web::Data<Tokens>>()?.get_ref().clone();
        let model = req.app_data::<web::Data<Model>>()?.get_ref().clone();
        let (server_tx, server_rx) = mpsc::channel(5);
        Some(Self {
            config,
            tokens,
            model,
            state: ConnectionState::Opening,
            server_tx,
            server_rx: Some(server_rx),
            lobby_token: Some(lobby_token),
            lobby: None,
            player_name,
            is_leader: false,
            identifier: "".into(),
        })
    }

    async fn validate_state(&mut self, addr: Addr<Self>) -> Result<(), ServerMessage> {
        if let Some(lobby_token) = self.lobby_token.take() {
            if let Some(lobby) = self.tokens.get_lobby(lobby_token).await {
                {
                    let mut lobby = lobby.lock().await;
                    match lobby.game_create_data.add_player(self.player_name.clone()) {
                        Ok(()) => {
                            log::info!("Player {} joined lobby {}", self.player_name, lobby.name);
                            let identifier = format!("{}\0{}", lobby.name, self.player_name);
                            self.identifier.push_str(&identifier);
                            if lobby.leader_name.is_none() {
                                self.is_leader = true;
                                lobby.leader_name = Some(self.player_name.clone());
                                log::info!("Player {} is the lobby leader", self.player_name);
                            }
                            let conn = GameLobbyConnection {
                                addr,
                                identifier,
                                player_name: self.player_name.clone(),
                            };
                            lobby.ws_connections.push(conn);
                            log::debug!("Added connection to game lobby");
                        }
                        Err(GameError::PlayerExists(_)) => {
                            return Err(ServerMessage::DuplicateName(
                                websocket::DuplicateNameMessage {},
                            ));
                        }
                        Err(e) => {
                            return Err(ServerMessage::Error(websocket::ErrorMessage {
                                message: format!("{}", e),
                            }));
                        }
                    }
                }
                self.lobby = Some(lobby);
            } else {
                return Err(ServerMessage::LobbyNotFound(
                    websocket::LobbyNotFoundMessage {},
                ));
            }
        } else {
            log::error!("BUG: connection started without lobby token");
        }
        Ok(())
    }

    async fn cleanup(&mut self) {
        if let Some(lobby) = &self.lobby {
            let mut lobby = lobby.lock().await;
            let _ = lobby
                .game_create_data
                .remove_player(self.player_name.clone());
            if let Some(game) = lobby.current_game.as_mut() {
                if let Ok(mut player) = game.game_data.player_by_name(self.player_name.clone()) {
                    player.kill();
                    game.game_data
                        .set_current_player(game.game_data.current_player_index());
                }
            }
            if let Some((idx, _)) = lobby
                .ws_connections
                .iter()
                .enumerate()
                .find(|(_, conn)| conn.identifier == self.identifier)
            {
                lobby.ws_connections.remove(idx);
                log::debug!("Removed connection from game lobby");
            }
            if self.is_leader {
                log::info!("Leader left the lobby {}", lobby.name);
                if let Some(conn) = lobby.ws_connections.first() {
                    log::info!("New leader: {}", conn.player_name);
                    let _ = conn.addr.try_send(InternalMessage::SetLeader);
                    let player = conn.player_name.clone();
                    lobby.leader_name.replace(player);
                } else {
                    lobby.leader_name.take();
                }
            }
        }
    }

    async fn build_submit_lobby(&self) -> Option<ServerMessage> {
        let lobby = self.lobby.as_ref()?;
        let lobby = lobby.lock().await;
        Some(ServerMessage::SubmitLobby(websocket::SubmitLobbyMessage {
            lobby_name: lobby.name.clone(),
        }))
    }

    async fn build_send_games(&self) -> Option<ServerMessage> {
        let lobby = self.lobby.as_ref()?;
        let lobby = lobby.lock().await;
        let game = lobby.current_game.as_ref()?;
        Some(ServerMessage::SendGames(websocket::SendGamesMessage {
            game: game.into(),
        }))
    }

    async fn build_submit_user(&self) -> Option<ServerMessage> {
        let lobby = self.lobby.as_ref()?;
        let lobby = lobby.lock().await;
        let players = lobby.game_create_data.players().clone();
        Some(ServerMessage::SubmitUser(websocket::SubmitUserMessage {
            user: players,
            leader: lobby.leader_name.as_ref().cloned().unwrap_or_default(),
        }))
    }

    async fn build_update_player(
        &self,
        update: Option<websocket::UpdatePlayerMessageUpdate>,
    ) -> Option<ServerMessage> {
        let lobby = self.lobby.as_ref()?;
        let lobby = lobby.lock().await;
        let game = lobby.current_game.as_ref()?;
        let current = if game.game_data.alive_player_count() > 0 {
            Some(game.game_data.current_player_name().into())
        } else {
            None
        };
        Some(ServerMessage::UpdatePlayer(
            websocket::UpdatePlayerMessage {
                alive: game.game_data.alive_players(),
                update,
                current,
            },
        ))
    }

    async fn build_init_response(&self) -> Option<ServerMessage> {
        let lobby = self.lobby.as_ref()?;
        let mut lobby = lobby.lock().await;
        let game = lobby.current_game.as_mut()?;
        let feed_idx = game.current_feed.unwrap();
        let feed = game.feed.get_mut(feed_idx).unwrap();
        let sequence_idx = feed.sequences.len() as u32 - 1;
        feed.continue_until = chrono::Local::now().add(*CONTINUE_DURATION);
        let submission = if sequence_idx == 0 {
            let token = if self.player_name == feed.user_submission.user {
                feed.user_submission.token.clone()
            } else {
                None
            };
            Some(websocket::InitResponseMessageUserSubmission {
                message: feed.user_submission.text.clone(),
                user: feed.user_submission.user.clone(),
                time: feed.user_submission.time,
                token,
            })
        } else {
            None
        };
        Some(ServerMessage::InitResponse(
            websocket::InitResponseMessage {
                feed: feed_idx as u32,
                sequence: sequence_idx,
                continue_until: feed.continue_until,
                user_submission: submission,
            },
        ))
    }

    async fn build_response_token(&self, token: String) -> Option<ServerMessage> {
        let lobby = self.lobby.as_ref()?;
        let mut lobby = lobby.lock().await;
        let game = lobby.current_game.as_mut()?;
        let feed_idx = game.current_feed.unwrap();
        let feed = game.feed.get_mut(feed_idx).unwrap();
        let sequence_idx = feed.sequences.len() as u32 - 1;
        let sequence = feed.sequences.last_mut().unwrap();
        sequence.text.push_str(&token);
        sequence.token_count += 1;
        Some(ServerMessage::ResponseToken(
            websocket::ResponseTokenMessage {
                feed: feed_idx as u32,
                sequence: sequence_idx,
                index: sequence.token_count - 1,
                text: token,
            },
        ))
    }

    async fn build_full_response(&self) -> Option<ServerMessage> {
        let lobby = self.lobby.as_ref()?;
        let mut lobby = lobby.lock().await;
        let game = lobby.current_game.as_mut()?;
        let feed_idx = game.current_feed.take().unwrap();
        let feed = game.feed.get_mut(feed_idx).unwrap();
        let sequence_idx = feed.sequences.len() as u32 - 1;
        let sequence = feed.sequences.last_mut().unwrap();
        sequence.timestamp = chrono::Local::now();
        sequence.finished = true;
        let history = game
            .game_data
            .history_mut()
            .iter_mut()
            .filter(|i| matches!(i, GameItem::Message(_)))
            .enumerate()
            .find(|(i, _)| *i == feed_idx);
        if let Some((_, GameItem::Message(msg))) = history {
            msg.add_response(GptResponse::new(sequence.text.clone(), sequence.timestamp));
        }
        game.last_player = game.game_data.current_player_index();
        game.game_data.next_player();
        log::debug!("Current game data: {:?}", game.game_data);
        Some(ServerMessage::FullResponse(
            websocket::FullResponseMessage {
                feed: feed_idx as u32,
                sequence: sequence_idx,
                text: sequence.text.clone(),
                time: sequence.timestamp,
            },
        ))
    }

    fn send_msg(&self, ctx: &mut <Self as Actor>::Context, msg: Option<ServerMessage>) {
        if let Some(msg) = msg {
            ctx.notify(ActixServerMessage(msg));
        }
    }

    async fn send_msg_async(&self, msg: Option<ServerMessage>) {
        if let Some(msg) = msg {
            if let Some(lobby) = &self.lobby {
                let msg = ActixServerMessage(msg);
                let lobby = lobby.lock().await;
                for conn in &lobby.ws_connections {
                    if conn.identifier != self.identifier {
                        continue;
                    }
                    match conn.addr.try_send(msg.clone()) {
                        Ok(()) => (),
                        Err(actix::prelude::SendError::Closed(_)) => {
                            log::error!("BUG: lobby contains closed connection");
                        }
                        Err(actix::prelude::SendError::Full(_)) => {
                            log::warn!("Web socket connection message queue is full!");
                        }
                    }
                }
            }
        }
    }

    async fn send_msg_all(&self, msg: Option<ServerMessage>) {
        if let Some(msg) = msg {
            if let Some(lobby) = &self.lobby {
                let msg = ActixServerMessage(msg);
                let lobby = lobby.lock().await;
                for conn in &lobby.ws_connections {
                    match conn.addr.try_send(msg.clone()) {
                        Ok(()) => (),
                        Err(actix::prelude::SendError::Closed(_)) => {
                            log::error!("BUG: lobby contains closed connection");
                        }
                        Err(actix::prelude::SendError::Full(_)) => {
                            log::warn!("Web socket connection message queue is full!");
                        }
                    }
                }
            }
        }
    }

    async fn send_msg_all_except_self(&self, msg: Option<ServerMessage>) {
        if let Some(msg) = msg {
            if let Some(lobby) = &self.lobby {
                let msg = ActixServerMessage(msg);
                let lobby = lobby.lock().await;
                for conn in &lobby.ws_connections {
                    if conn.identifier == self.identifier {
                        continue;
                    }
                    match conn.addr.try_send(msg.clone()) {
                        Ok(()) => (),
                        Err(actix::prelude::SendError::Closed(_)) => {
                            log::error!("BUG: lobby contains closed connection");
                        }
                        Err(actix::prelude::SendError::Full(_)) => {
                            log::warn!("Web socket connection message queue is full!");
                        }
                    }
                }
            }
        }
    }

    async fn handle_client_message(
        &mut self,
        client_message: ClientMessage,
    ) -> ClientMessageResult {
        if self.lobby.is_none() {
            log::error!("BUG: No lobby in open connection");
            return ClientMessageResult::Ok;
        }
        let game_started = {
            let lobby = self.lobby.as_ref().unwrap().lock().await;
            lobby.current_game.is_some()
        };
        match client_message {
            ClientMessage::CreateGame(_) if !game_started && self.is_leader => {
                let msg = {
                    let mut lobby = self.lobby.as_ref().unwrap().lock().await;
                    let game = GameData::new(lobby.game_create_data.clone());
                    let player_len = game.players().len();
                    let game = Game {
                        game_data: game,
                        feed: Vec::new(),
                        current_feed: None,
                        banned_users: Vec::new(),
                        last_player: player_len - 1,
                    };
                    let msg = websocket::GameStartedMessage {
                        user: game.game_data.players().clone(),
                    };
                    lobby.current_game = Some(game);
                    msg
                };
                self.send_msg_all(Some(ServerMessage::GameStarted(msg)))
                    .await;
                self.send_msg_all(self.build_update_player(None).await)
                    .await;
            }
            ClientMessage::PromoteLeader(msg) if self.is_leader => {
                let mut lobby = self.lobby.as_ref().unwrap().lock().await;
                if !lobby.game_create_data.players().contains(&msg.user) {
                    drop(lobby);
                    self.send_msg_async(Some(ServerMessage::error("unknown player")))
                        .await;
                    return ClientMessageResult::Ok;
                }
                log::info!(
                    "Promoting user {} in lobby {} to leader",
                    msg.user,
                    lobby.name
                );
                lobby.leader_name = Some(msg.user);
                drop(lobby);
                self.send_msg_all(self.build_submit_user().await).await;
            }
            ClientMessage::KickUser(msg) if self.is_leader => {
                let mut lobby = self.lobby.as_ref().unwrap().lock().await;
                if !lobby.game_create_data.players().contains(&msg.user) {
                    drop(lobby);
                    self.send_msg_async(Some(ServerMessage::error("unknown player")))
                        .await;
                    return ClientMessageResult::Ok;
                }
                log::info!("Kicking user {} from lobby {}", msg.user, lobby.name);
                let _ = lobby.game_create_data.remove_player(msg.user.clone());
                if let Some(game) = lobby.current_game.as_mut() {
                    game.banned_users.push(msg.user.clone());
                    if let Ok(mut user) = game.game_data.player_by_name(msg.user.clone()) {
                        user.kill();
                    }
                }
                let conn = lobby
                    .ws_connections
                    .iter()
                    .find(|c| c.player_name == msg.user);
                if let Some(conn) = conn {
                    if let Err(e) = conn.addr.try_send(ConnectionStreamMessage::Close) {
                        log::error!("Failed to send close message to kicked user: {}", e);
                    }
                }
                drop(lobby);
                self.send_msg_all(self.build_submit_user().await).await;
                self.send_msg_all(self.build_update_player(None).await)
                    .await;
            }
            ClientMessage::ReorderUser(msg) if self.is_leader => {
                let mut lobby = self.lobby.as_ref().unwrap().lock().await;
                if !lobby.game_create_data.players().contains(&msg.user) {
                    drop(lobby);
                    self.send_msg_async(Some(ServerMessage::error("unknown player")))
                        .await;
                    return ClientMessageResult::Ok;
                }
                let players = lobby.game_create_data.players_mut();
                let player_idx = players
                    .iter()
                    .enumerate()
                    .find(|(_, p)| *p == &msg.user)
                    .unwrap()
                    .0;
                let len = players.len() as i32;
                let mut target_idx = msg.index;
                if msg.relative {
                    target_idx += player_idx as i32;
                } else if target_idx < 0 {
                    target_idx += len;
                };
                let target_idx = target_idx.clamp(0, len - 1) as usize;
                players.remove(player_idx);
                players.insert(target_idx, msg.user.clone());
                drop(lobby);
                self.send_msg_all(self.build_submit_user().await).await;
            }
            ClientMessage::SubmitMessage(msg) if game_started => {
                let mut lobby = self.lobby.as_ref().unwrap().lock().await;
                let game = lobby.current_game.as_mut().unwrap();
                if game.current_feed.is_some() {
                    drop(lobby);
                    self.send_msg_async(Some(ServerMessage::error(
                        "generation already in progress",
                    )))
                    .await;
                    return ClientMessageResult::Ok;
                }
                if let Err(e) = game
                    .game_data
                    .add_message(msg.user.clone(), msg.message.clone())
                {
                    drop(lobby);
                    self.send_msg_async(Some(ServerMessage::error(format!("{}", e))))
                        .await;
                    return ClientMessageResult::Ok;
                }
                let now = chrono::Local::now();
                game.feed.push(GameFeed {
                    sequences: vec![GameFeedSequence {
                        text: "".into(),
                        timestamp: now,
                        token_count: 0,
                        finished: false,
                    }],
                    continue_until: now.add(*CONTINUE_DURATION),
                    user_submission: UserSubmission {
                        token: msg.token,
                        user: msg.user,
                        text: msg.message,
                        time: now,
                    },
                    generator: None,
                });
                let feed = game.feed.last_mut().unwrap();
                let text = game.game_data.current_text().into();
                let generator = match self
                    .model
                    .create_token_generator(text, self.config.generation_options)
                    .await
                {
                    Ok(gen) => gen,
                    Err(e) => {
                        drop(lobby);
                        log::error!("Error creating token generator: {}", e);
                        self.send_msg_async(Some(ServerMessage::error("Internal server error")))
                            .await;
                        return ClientMessageResult::Ok;
                    }
                };
                let stream = generator
                    .get_stream()
                    .map(|token| GeneratorTokenMessage { token })
                    .boxed();
                feed.generator = Some(generator);
                game.current_feed = Some(game.feed.len() - 1);
                return ClientMessageResult::AddStream(stream);
            }
            ClientMessage::NotifyTyping(msg) if game_started => {
                self.send_msg_all_except_self(Some(ServerMessage::NotifyTyping(msg)))
                    .await
            }
            ClientMessage::ContinueResponse(msg) if game_started => {
                let mut lobby = self.lobby.as_ref().unwrap().lock().await;
                let game = lobby.current_game.as_mut().unwrap();
                if game.current_feed.is_some() {
                    drop(lobby);
                    self.send_msg_async(Some(ServerMessage::error(
                        "generation already in progress",
                    )))
                    .await;
                    return ClientMessageResult::Ok;
                }
                let feed = if let Some(feed) = game.feed.get_mut(msg.feed as usize) {
                    feed
                } else {
                    drop(lobby);
                    self.send_msg_async(Some(ServerMessage::error("wrong feed")))
                        .await;
                    return ClientMessageResult::Ok;
                };
                let generator = if let Some(generator) = feed.generator.as_mut() {
                    generator
                } else {
                    drop(lobby);
                    log::error!("BUG: feed doesn't contain generator");
                    self.send_msg_async(Some(ServerMessage::error("Internal server error")))
                        .await;
                    return ClientMessageResult::Ok;
                };
                if let Err(e) = generator.reset().await {
                    log::error!("BUG: failed to reset generator: {}", e);
                }
                feed.sequences.push(GameFeedSequence {
                    finished: false,
                    text: "".into(),
                    timestamp: chrono::Local::now(),
                    token_count: 0,
                });
                let stream = generator
                    .get_stream()
                    .map(|token| GeneratorTokenMessage { token })
                    .boxed();
                game.current_feed = Some(msg.feed as usize);
                return ClientMessageResult::AddStream(stream);
            }
            ClientMessage::KillPlayer(msg) if game_started => {
                let mut lobby = self.lobby.as_ref().unwrap().lock().await;
                let game = lobby.current_game.as_mut().unwrap();
                match game.game_data.player_by_name(msg.user) {
                    Ok(mut player) => {
                        player.kill();
                    }
                    Err(e) => {
                        drop(lobby);
                        self.send_msg_async(Some(ServerMessage::error(format!("{}", e))))
                            .await;
                        return ClientMessageResult::Ok;
                    }
                }
                // update player index
                game.game_data
                    .set_current_player(game.game_data.current_player_index());
                drop(lobby);
                self.send_msg_all(
                    self.build_update_player(Some(websocket::UpdatePlayerMessageUpdate {
                        user: self.player_name.clone(),
                        update_type: websocket::UpdatePlayerMessageUpdateType::Kill,
                    }))
                    .await,
                )
                .await;
            }
            ClientMessage::RevivePlayer(msg) if game_started => {
                let mut lobby = self.lobby.as_ref().unwrap().lock().await;
                let game = lobby.current_game.as_mut().unwrap();
                if game.banned_users.contains(&msg.user) {
                    drop(lobby);
                    self.send_msg_async(Some(ServerMessage::error(
                        "User was kicked from the game and can't be revived",
                    )))
                    .await;
                    return ClientMessageResult::Ok;
                }
                match game.game_data.player_by_name(msg.user.clone()) {
                    Ok(mut player) => {
                        player.revive();
                    }
                    Err(e) => {
                        drop(lobby);
                        self.send_msg_async(Some(ServerMessage::error(format!("{}", e))))
                            .await;
                        return ClientMessageResult::Ok;
                    }
                }
                // make just revived player the active player if they're the only player
                if game.game_data.alive_player_count() == 1 {
                    game.game_data.set_current_player(0);
                }
                // if the revived player would have been the next player after the last player, but
                // was skipped because they were dead, set them as current player
                if !game.feed.is_empty() {
                    let prev_idx = game.last_player;
                    let prev_name = game.game_data.players().get(prev_idx).unwrap();
                    // check that now current player hasn't written anything yet
                    if &game.feed.last().unwrap().user_submission.user == prev_name {
                        game.game_data.set_current_player(prev_idx + 1);
                    }
                }
                drop(lobby);
                self.send_msg_all(
                    self.build_update_player(Some(websocket::UpdatePlayerMessageUpdate {
                        user: self.player_name.clone(),
                        update_type: websocket::UpdatePlayerMessageUpdateType::Revive,
                    }))
                    .await,
                )
                .await;
            }
            ClientMessage::SkipPlayer(msg) if game_started => {
                let mut lobby = self.lobby.as_ref().unwrap().lock().await;
                let game = lobby.current_game.as_mut().unwrap();
                if game.game_data.current_player_name() == msg.user {
                    game.game_data.next_player();
                    drop(lobby);
                    self.send_msg_all(self.build_update_player(None).await)
                        .await;
                }
            }
            ClientMessage::GameFinished(_) if game_started => {
                let mut lobby = self.lobby.as_ref().unwrap().lock().await;
                let game = lobby.current_game.as_mut().unwrap();
                if game.current_feed.is_some() {
                    drop(lobby);
                    self.send_msg_async(Some(ServerMessage::error(
                        "generation already in progress",
                    )))
                    .await;
                    return ClientMessageResult::Ok;
                }
                game.game_data.end_game();
                drop(lobby);
                self.send_msg_all(Some(ServerMessage::GameFinished(
                    websocket::GameFinishedMessage {},
                )))
                .await;
                self.send_msg_all(self.build_update_player(None).await)
                    .await;
            }
            _ => {
                self.send_msg_async(Some(ServerMessage::error("unsupported")))
                    .await;
            }
        }
        ClientMessageResult::Ok
    }
}

impl Actor for Connection {
    type Context = ws::WebsocketContext<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        log::info!("Web socket connection started");
        if let Some(rx) = self.server_rx.take() {
            ctx.add_message_stream(rx.map(ActixServerMessage));
        } else {
            log::error!("BUG: Web socket actor started without receiver!");
        }
        let addr = ctx.address();
        self.state = ConnectionState::Open;
        match block_on(self.validate_state(addr)) {
            Ok(()) => {
                self.send_msg(ctx, block_on(self.build_submit_lobby()));
                self.send_msg(ctx, block_on(self.build_send_games()));
                block_on(self.send_msg_all(block_on(self.build_submit_user())));
                block_on(self.send_msg_all(block_on(self.build_update_player(None))));
            }
            Err(msg) => {
                <Self as Handler<ActixServerMessage>>::handle(self, ActixServerMessage(msg), ctx);
                ctx.notify(ConnectionStreamMessage::Close);
            }
        }
    }

    fn stopping(&mut self, _ctx: &mut Self::Context) -> Running {
        log::trace!("Web socket actor stopping");
        if self.state != ConnectionState::Closed {
            log::warn!("Web socket connection wasn't closed properly");
        }
        // cleanup
        block_on(self.cleanup());
        block_on(self.send_msg_all(block_on(self.build_submit_user())));
        block_on(self.send_msg_all(block_on(self.build_update_player(None))));
        log::info!("Web socket connection stopped");
        Running::Stop
    }
}

impl StreamHandler<GeneratorTokenMessage> for Connection {
    fn handle(&mut self, item: GeneratorTokenMessage, _ctx: &mut Self::Context) {
        let GeneratorTokenMessage { token } = item;
        block_on(self.send_msg_all(block_on(self.build_response_token(token))));
    }

    fn started(&mut self, _ctx: &mut Self::Context) {
        block_on(self.send_msg_all(block_on(self.build_init_response())));
    }

    fn finished(&mut self, _ctx: &mut Self::Context) {
        block_on(self.send_msg_all(block_on(self.build_full_response())));
        block_on(self.send_msg_all(block_on(self.build_update_player(None))));
    }
}

impl StreamHandler<Result<ws::Message, ws::ProtocolError>> for Connection {
    fn handle(&mut self, item: Result<Message, ProtocolError>, ctx: &mut Self::Context) {
        log::debug!("Handling incoming web socket message: {:?}", item);
        match item {
            Ok(Message::Text(msg)) if self.state == ConnectionState::Open => {
                match serde_json::from_str(&msg) {
                    Err(e) => log::warn!("Received invalid web socket message from client: {}", e),
                    Ok(client_message) => {
                        match block_on(self.handle_client_message(client_message)) {
                            ClientMessageResult::AddStream(stream) => {
                                ctx.add_stream(stream);
                            }
                            _ => (),
                        }
                    }
                }
            }
            Ok(Message::Close(reason)) => {
                match self.state {
                    ConnectionState::ServerClosed => {
                        log::debug!("Received return close message");
                        // client close message received, stop connection
                        self.state = ConnectionState::Closed;
                        ctx.stop();
                    }
                    ConnectionState::Open => {
                        log::info!("Web socket connection closed by client: {:?}", reason);
                        log::debug!("Received close message on open connection");
                        // client close message received, send server close message
                        self.state = ConnectionState::ClientClosed;
                        ctx.notify(ConnectionStreamMessage::Close);
                    }
                    _ => (),
                }
            }
            Err(e) => {
                log::error!("Protocol error on web socket: {}", e);
            }
            _ => (),
        }
    }
}

impl actix::Message for ConnectionStreamMessage {
    type Result = ();
}

impl Handler<ConnectionStreamMessage> for Connection {
    type Result = ();

    fn handle(&mut self, item: ConnectionStreamMessage, ctx: &mut Self::Context) -> Self::Result {
        log::debug!("Sending web socket message: {:?}", item);
        match item {
            ConnectionStreamMessage::Text(text) if self.state == ConnectionState::Open => {
                ctx.text(text)
            }
            ConnectionStreamMessage::Close => {
                match self.state {
                    ConnectionState::Open => {
                        log::info!("Web socket connection closing by server");
                        log::debug!("Sending close message on open connection");
                        // send close message to client and wait for client's close message
                        self.state = ConnectionState::ServerClosed;
                        ctx.close(None);
                    }
                    ConnectionState::ClientClosed => {
                        log::debug!("Sending return close message");
                        // send close message to client, stop connection
                        self.state = ConnectionState::Closed;
                        ctx.close(None);
                        ctx.stop();
                    }
                    _ => (),
                }
            }
            _ => (),
        }
    }
}

impl actix::Message for ActixServerMessage {
    type Result = ();
}

impl Handler<ActixServerMessage> for Connection {
    type Result = ();

    fn handle(&mut self, msg: ActixServerMessage, ctx: &mut Self::Context) -> Self::Result {
        log::debug!("Sending message: {:?}", msg);
        match serde_json::to_string(&msg.0) {
            Ok(text) => ctx.notify(ConnectionStreamMessage::Text(text)),
            Err(e) => {
                log::error!("Failed serializing server message {:?}: {}", msg, e);
            }
        }
    }
}

impl actix::Message for InternalMessage {
    type Result = ();
}

impl Handler<InternalMessage> for Connection {
    type Result = ();

    fn handle(&mut self, msg: InternalMessage, _ctx: &mut Self::Context) -> Self::Result {
        match msg {
            InternalMessage::SetLeader => {
                self.is_leader = true;
            }
        }
    }
}
