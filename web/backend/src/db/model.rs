use super::{
    schema::{tokens, users},
    DBError, Model,
};
use crate::table_struct;
use diesel::prelude::*;

table_struct! {
    User, NewUser -> "users", users {
        name: String = String::from(""),
        password: String = String::from(""),
        is_admin: bool = false,
    }

    fn get(name: String);
}

table_struct! {
    Token, NewToken -> "tokens", tokens {
        token: String = String::from(""),
        creating_user: String = String::from(""),
        expiry: chrono::NaiveDateTime = current_datetime(),
    }

    fn get(token: String);
}

pub fn current_datetime() -> chrono::NaiveDateTime {
    chrono::Utc::now().naive_utc()
}
