use gpt::model::{GenerationOptions, ModelType};
use serde::Deserialize;
use std::path::Path;

#[derive(Debug, Clone, Deserialize)]
pub struct Config {
    #[serde(default)]
    pub api: Api,
    #[serde(default)]
    pub secret_key: Option<String>,
    #[serde(default)]
    pub db_path: Option<String>,
    #[serde(default)]
    pub admin_password: Option<String>,
    #[serde(default = "default_model_type")]
    pub model_type: ModelType,
    #[serde(default)]
    pub generation_options: GenerationOptions,
    #[serde(default)]
    pub static_file_root: Option<String>,
    #[serde(default = "default_token_length")]
    pub token_length: usize,
}

#[derive(Debug, Default, Clone, Deserialize)]
pub struct Api {
    #[serde(default)]
    pub allowed_keys: Vec<String>,
}

pub fn load_config(path: &Path) -> Result<Config, anyhow::Error> {
    if path.exists() {
        let text = std::fs::read_to_string(path)?;
        Ok(toml::from_str(&text)?)
    } else {
        log::warn!("No config file found, using default config");
        Ok(toml::from_str("")?)
    }
}

fn default_model_type() -> ModelType {
    ModelType::Gpt2Large
}

fn default_token_length() -> usize {
    4
}
