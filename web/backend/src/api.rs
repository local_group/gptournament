mod gpt;
pub mod helper;
mod user;

use ::gpt::model::AsyncModelTokenGenerator;
use actix_web::web;

pub fn config(cfg: &mut web::ServiceConfig) {
    cfg.service(
        web::resource("/generator/create")
            .route(web::get().to(gpt::create_generator_get))
            .route(web::post().to(gpt::create_generator_post)),
    )
    .service(
        web::resource("/generator/generate")
            .route(web::get().to(gpt::generate::<AsyncModelTokenGenerator>)),
    )
    .service(web::resource("/game/create_lobby").route(web::get().to(gpt::create_lobby)))
    .service(web::resource("/stream").route(web::get().to(gpt::stream)))
    .service(
        web::resource("/login")
            .route(
                web::post()
                    .guard(helper::guards::content_type(
                        "application/x-www-form-urlencoded",
                    ))
                    .to(user::login),
            )
            .route(
                web::post()
                    .guard(helper::guards::content_type("application/json"))
                    .to(user::login_json),
            ),
    )
    .service(web::resource("/user_info").route(web::get().to(user::user_info)))
    .service(
        web::resource("/add_user")
            .route(web::get().to(user::add_user))
            .route(web::post().to(user::add_user_token)),
    )
    .service(web::resource("/list_users").route(web::get().to(user::list_users)));
}
