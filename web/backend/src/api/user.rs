use super::helper::{
    self,
    cookie::Cookies,
    guards::{Admin, Authentication, ClientIp},
    types::{
        StatusResponse, TokenResponse, UserInfo, UserListResponse, UserLogin, UserType,
        UserTypeResponse,
    },
};
use crate::db::{self, DbConn};
use actix_web::web;
use cookie::Cookie;
use libreauth::key::KeyBuilder;
use std::ops::Add;
use web_common::types::{request, response};

pub async fn login(
    client_ip: ClientIp,
    login: web::Form<UserLogin>,
    cookies: Cookies,
    db: DbConn,
) -> StatusResponse {
    let login = login.into_inner();
    if let Some(user_data) = helper::validate_user(login, client_ip, &db) {
        cookies.add_secret(Cookie::new(
            "user",
            serde_json::to_string(&user_data).unwrap(),
        ));
        StatusResponse::Ok
    } else {
        StatusResponse::error("invalid username or password")
    }
}

pub async fn login_json(
    client_ip: ClientIp,
    login: web::Json<UserLogin>,
    cookies: Cookies,
    db: DbConn,
) -> TokenResponse {
    let login = login.into_inner();
    if let Some(user_data) = helper::validate_user(login, client_ip, &db) {
        cookies.add_secret(Cookie::new(
            "user",
            serde_json::to_string(&user_data).unwrap(),
        ));
        let cookie = cookies.get("user").unwrap();
        let token = cookie.value().to_owned();
        cookies.remove_secret(cookie.clone());
        TokenResponse::ok(token)
    } else {
        TokenResponse::error("invalid username or password")
    }
}

pub async fn user_info(auth: Option<Authentication>) -> UserTypeResponse {
    if let Some(auth) = auth {
        match auth {
            Authentication::APIKey(_) => UserTypeResponse::ok(UserType::ApiKey),
            Authentication::User(user) | Authentication::Admin(Admin(user)) => {
                UserTypeResponse::ok(UserType::User(UserInfo {
                    username: user.username,
                    is_admin: user.is_admin,
                }))
            }
        }
    } else {
        UserTypeResponse::ok(UserType::NotLoggedIn)
    }
}

pub async fn add_user(admin: Admin, db: DbConn) -> TokenResponse {
    let token = KeyBuilder::new().size(8).as_hex();
    let expiry = db::model::current_datetime().add(chrono::Duration::minutes(10));
    let token = db::model::Token::new_from_values(token, admin.0.username, expiry);
    match db.add(&token) {
        Ok(_) => TokenResponse::ok(token.token),
        Err(e) => {
            log::error!("error inserting new token into db: {}", e);
            TokenResponse::error("failed to get token")
        }
    }
}

pub async fn add_user_token(
    token: web::Query<request::Token>,
    login: web::Form<UserLogin>,
    db: DbConn,
) -> StatusResponse {
    let token = match db.get_token(&token.token) {
        Ok(token) => token,
        Err(db::DBError::NoResults) => {
            return StatusResponse::error("invalid token");
        }
        Err(e) => {
            log::error!("error getting token from db: {}", e);
            return StatusResponse::error("internal error");
        }
    };
    if token.expiry < db::model::current_datetime() {
        return StatusResponse::error("token expired");
    }
    let password = helper::build_hasher().hash(&login.password).unwrap();
    let new_user = db::model::User::new_from_values(login.into_inner().username, password, false);
    match db.add(&new_user) {
        Ok(_) => StatusResponse::Ok,
        Err(db::DBError::UniqueViolation(_)) => StatusResponse::error("username already taken"),
        Err(e) => {
            log::error!("error adding new user to db: {}", e);
            StatusResponse::error("internal error")
        }
    }
}

pub async fn list_users(_admin: Admin, db: DbConn) -> UserListResponse {
    match db.get_users() {
        Err(e) => {
            log::error!("error fetching users from db: {}", e);
            UserListResponse::error("failed to fetch users")
        }
        Ok(users) => UserListResponse::ok(
            users
                .into_iter()
                .map(|u| response::User { username: u.name })
                .collect::<Vec<_>>(),
        ),
    }
}
