use crate::state::game::Game;
use gpt::game::state::GameItem;
use web_common::types::websocket::*;

impl From<&'_ Game> for SendGamesMessageGame {
    fn from(game: &Game) -> Self {
        let mut items = Vec::new();
        let mut feed_idx = 0;
        for h in game.game_data.history() {
            match h {
                GameItem::Message(msg) => {
                    let feed = game.feed.get(feed_idx).expect("BUG: invalid feed index");
                    items.push(SendGamesMessageGameItem::Message(
                        SendGamesMessageGameItemMessage {
                            sender: SendGamesMessageGameItemMessageSender {
                                user: msg.sender().into(),
                                message: msg.text().into(),
                                time: *msg.timestamp(),
                            },
                            response: SendGamesMessageGameItemMessageResponse {
                                feed: feed_idx as u32,
                                continue_until: feed.continue_until,
                                sequence: msg
                                    .responses()
                                    .iter()
                                    .map(|response| {
                                        SendGamesMessageGameItemMessageResponseSequence {
                                            time: *response.timestamp(),
                                            message: response.text().into(),
                                        }
                                    })
                                    .collect(),
                            },
                        },
                    ));
                    feed_idx += 1;
                }
                GameItem::Kill(kill) => {
                    items.push(SendGamesMessageGameItem::Kill(
                        SendGamesMessageGameItemKill {
                            user: kill.player().into(),
                        },
                    ));
                }
                GameItem::Revive(revive) => {
                    items.push(SendGamesMessageGameItem::Revive(
                        SendGamesMessageGameItemRevive {
                            user: revive.player().into(),
                        },
                    ));
                }
            }
        }
        Self {
            user: game.game_data.players().clone(),
            items,
        }
    }
}
