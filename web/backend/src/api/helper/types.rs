pub mod websocket;

pub use web_common::types::request::*;
pub use web_common::types::response::*;

use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
pub struct UserData {
    pub(crate) username: String,
    pub(crate) client_ip: String,
}
