use super::cookie::Cookies;
use super::types::UserData;
use crate::{
    config::Config,
    db::{DbConn, DbPool},
    error::GuardError,
    from_request,
};
use actix_web::{
    dev::{Payload, PayloadStream, RequestHead},
    guard::Guard,
    http::header::Header,
    web::Data,
    HttpRequest,
};
use actix_web_httpauth::headers::authorization::{Authorization, Bearer};
use cookie::Cookie;
use futures_util::future::{ready, Ready};

pub(crate) trait FromRequestSync: Sized {
    fn from_request_sync(req: &HttpRequest) -> Result<Self, GuardError>;
}

pub(crate) trait TryFromRequestSync: Sized {
    fn try_from_request_sync(req: &HttpRequest) -> Result<Self, GuardError>;
}

pub fn content_type(value: &'static str) -> ContentTypeGuard {
    ContentTypeGuard(value)
}

pub struct ContentTypeGuard(&'static str);

impl Guard for ContentTypeGuard {
    fn check(&self, req: &RequestHead) -> bool {
        if let Some(val) = req
            .headers
            .get("content-type")
            .and_then(|h| h.to_str().ok())
        {
            let idx = val.find(';');
            let str = idx.map_or_else(|| val, |i| val.split_at(i).0);
            return str == self.0;
        }
        false
    }
}

impl<T: 'static + TryFromRequestSync + Clone> FromRequestSync for T {
    fn from_request_sync(req: &HttpRequest) -> Result<Self, GuardError> {
        if req.extensions().get::<Result<Self, GuardError>>().is_none() {
            log::trace!(
                "Trying to get resource of type {}",
                std::any::type_name::<Self>()
            );
            let s = Self::try_from_request_sync(req);
            req.extensions_mut().insert(s);
        }
        req.extensions()
            .get::<Result<Self, GuardError>>()
            .unwrap()
            .clone()
    }
}

#[derive(Clone, Debug)]
pub struct ApiKey {
    _key: String,
}

impl ApiKey {
    pub fn key_is_valid(config: &Config, key: &str) -> bool {
        config.api.allowed_keys.iter().any(|k| k == key)
    }
}

impl TryFromRequestSync for ApiKey {
    fn try_from_request_sync(req: &HttpRequest) -> Result<Self, GuardError> {
        log::trace!("Trying to get API key authentication");
        let config = req.app_data::<Data<Config>>().unwrap();
        let header = req
            .headers()
            .get("x-api-key")
            .map(|header| header.to_str().ok())
            .flatten()
            .ok_or(GuardError::Unauthenticated)?;
        if Self::key_is_valid(&config, header) {
            Ok(Self {
                _key: header.into(),
            })
        } else {
            Err(GuardError::Unauthenticated)
        }
    }
}

#[derive(Clone, Debug)]
pub struct User {
    pub(crate) username: String,
    pub(crate) is_admin: bool,
}

#[derive(Clone, Debug)]
pub struct Admin(pub(crate) User);

impl User {
    fn from_model(user: crate::db::model::User) -> Self {
        Self {
            username: user.name,
            is_admin: user.is_admin,
        }
    }

    pub fn try_from_user_data(
        user: UserData,
        request_ip: &ClientIp,
        db: &DbConn,
    ) -> Result<Self, GuardError> {
        if user.client_ip != request_ip.0 {
            return Err(GuardError::Unauthenticated);
        }

        let user = match db.get_user(&user.username) {
            Ok(user) => Self::from_model(user),
            Err(e) => {
                log::error!("Error querying user from db: {}", e);
                return Err(GuardError::InternalServerError);
            }
        };
        Ok(user)
    }
}

impl TryFromRequestSync for User {
    fn try_from_request_sync(req: &HttpRequest) -> Result<Self, GuardError> {
        log::trace!("Trying to get user authentication");
        let cookies = Cookies::from_request_sync(req)?;
        let header = Authorization::<Bearer>::parse(req)
            .ok()
            .map(|auth| auth.into_scheme());
        let mut cookie = cookies.get_secret("user");
        if let (None, Some(header)) = (cookie.as_ref(), header.as_ref()) {
            cookies.add(Cookie::new("user", header.token().clone()));
            cookie = cookies.get_secret("user");
            cookies.remove(cookie.as_ref().cloned().unwrap());
        }
        let user = cookie
            .as_ref()
            .map(|c| c.value())
            .and_then(|value| serde_json::from_str::<UserData>(value).ok())
            .ok_or(GuardError::Unauthenticated)?;

        let client_ip = ClientIp::from_request_sync(req)?;
        let db = DbConn::from_request_sync(req)?;

        Self::try_from_user_data(user, &client_ip, &db)
    }
}

impl TryFromRequestSync for Admin {
    fn try_from_request_sync(req: &HttpRequest) -> Result<Self, GuardError> {
        log::trace!("Trying to get admin authentication");
        let user = User::from_request_sync(req)?;
        if user.is_admin {
            Ok(Self(user))
        } else {
            Err(GuardError::Unauthenticated)
        }
    }
}

#[derive(Debug, Clone)]
pub enum Authentication {
    APIKey(ApiKey),
    User(User),
    Admin(Admin),
}

impl TryFromRequestSync for Authentication {
    fn try_from_request_sync(req: &HttpRequest) -> Result<Self, GuardError> {
        if let Ok(admin) = Admin::from_request_sync(req) {
            Ok(Self::Admin(admin))
        } else if let Ok(user) = User::from_request_sync(req) {
            Ok(Self::User(user))
        } else if let Ok(api_key) = ApiKey::from_request_sync(req) {
            Ok(Self::APIKey(api_key))
        } else {
            Err(GuardError::Unauthenticated)
        }
    }
}

#[derive(Clone, Debug)]
pub struct ClientIp(pub(crate) String);

impl FromRequestSync for ClientIp {
    fn from_request_sync(req: &HttpRequest) -> Result<Self, GuardError> {
        log::trace!("Getting client IP from request");
        let ip = req
            .connection_info()
            .realip_remote_addr()
            .ok_or(GuardError::InternalServerError)
            .map(|ip| {
                let mut ip = ip;
                if let Some(sep) = ip.find(':') {
                    ip = &ip[..sep];
                }
                ClientIp(ip.into())
            });
        if let Ok(ip) = &ip {
            log::trace!("Request client IP: {:?}", ip);
        }
        ip
    }
}

impl FromRequestSync for Cookies {
    fn from_request_sync(req: &HttpRequest) -> Result<Self, GuardError> {
        log::trace!("Getting cookie store for request");
        req.extensions().get::<Cookies>().map_or_else(
            || {
                log::error!("Couldn't get cookie store from request! Maybe the cookie middleware didn't run?");
                Err(GuardError::InternalServerError)
            },
            |cookies| Ok(cookies.clone()),
        )
    }
}

impl FromRequestSync for DbConn {
    fn from_request_sync(req: &HttpRequest) -> Result<Self, GuardError> {
        log::trace!("Getting db conn for request");
        let db = req
            .app_data::<Data<DbPool>>()
            .map(|pool| DbConn::get(&pool).ok())
            .flatten()
            .ok_or(GuardError::InternalServerError);
        db
    }
}

from_request!(ApiKey);
from_request!(User);
from_request!(Admin);
from_request!(Authentication);
from_request!(ClientIp);
from_request!(Cookies);
from_request!(DbConn);
