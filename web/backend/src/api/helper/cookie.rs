use actix_http::http::{header, HeaderMap, HeaderValue};
use cookie::{Cookie, CookieJar, Key};
use std::sync::{Arc, Mutex};

#[derive(Clone)]
pub struct CookieBuilder {
    key: Key,
}

impl CookieBuilder {
    pub fn new(key: Option<String>) -> Result<Self, anyhow::Error> {
        let key = if let Some(key) = key {
            let key = base64::decode(key)?;
            Key::derive_from(&key)
        } else {
            log::warn!("Generating cookie encryption key");
            Key::generate()
        };
        Ok(Self { key })
    }

    pub fn new_from_jar(&self, jar: CookieJar) -> Cookies {
        Cookies {
            key: self.key.clone(),
            jar: Arc::new(Mutex::new(jar)),
        }
    }
}

#[derive(Clone)]
pub struct Cookies {
    key: Key,
    jar: Arc<Mutex<CookieJar>>,
}

impl Cookies {
    pub fn add(&self, cookie: Cookie<'static>) {
        let mut jar = self.jar.lock().unwrap();
        jar.add(cookie);
    }

    pub fn add_secret(&self, cookie: Cookie<'static>) {
        let mut jar = self.jar.lock().unwrap();
        jar.private(&self.key).add(cookie);
    }

    pub fn remove(&self, cookie: Cookie<'static>) {
        let mut jar = self.jar.lock().unwrap();
        jar.remove(cookie);
    }

    pub fn remove_secret(&self, cookie: Cookie<'static>) {
        let mut jar = self.jar.lock().unwrap();
        jar.private(&self.key).remove(cookie);
    }

    pub fn get(&self, name: &str) -> Option<Cookie<'static>> {
        let jar = self.jar.lock().unwrap();
        jar.get(name).map(Clone::clone)
    }

    pub fn get_secret(&self, name: &str) -> Option<Cookie<'static>> {
        let mut jar = self.jar.lock().unwrap();
        jar.private(&self.key).get(name)
    }

    pub fn insert_into_headers(&self, headers: &mut HeaderMap) {
        let jar = self.jar.lock().unwrap();
        for cookie in jar.delta() {
            match HeaderValue::from_str(&cookie.to_string()) {
                Ok(val) => headers.append(header::SET_COOKIE, val),
                Err(e) => {
                    log::warn!(
                        "Invalid cookie delta {:?} in response! Error: {}",
                        cookie,
                        e
                    );
                }
            }
        }
    }
}
