pub mod cookie;
pub mod generator;
pub mod guards;
mod macros;
pub mod types;

use crate::db::DbConn;
use guards::ClientIp;
use libreauth::pass::{HashBuilder, Hasher};
use types::{UserData, UserLogin};

pub fn build_hasher() -> Hasher {
    HashBuilder::new().finalize().unwrap()
}

pub fn validate_user(login: UserLogin, client_ip: ClientIp, db: &DbConn) -> Option<UserData> {
    if let Ok(user) = db.get_user(&login.username) {
        let hasher = HashBuilder::from_phc(&user.password).ok()?;
        if hasher.is_valid(&login.password) {
            Some(UserData {
                username: login.username,
                client_ip: client_ip.0,
            })
        } else {
            None
        }
    } else {
        None
    }
}
