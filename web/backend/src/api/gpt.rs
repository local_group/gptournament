use super::helper::{
    generator::{ResetGenerator, StreamedStateGenerator},
    guards::Authentication,
    types::{IdResponse, Response, StatusResponse, TokenResponse},
};
use crate::{
    config::Config,
    resources::Model,
    state::{game::GameLobby, GameLobbies, Generators, ModelState, Token, Tokens},
    websocket::Connection,
};
use actix_web::web::Bytes;
use actix_web::{web, Either, HttpRequest, HttpResponse};
use actix_web_actors::ws;
use futures_util::stream::Stream;
use futures_util::StreamExt;
use gpt::game::state::GameCreateData;
use web_common::types::request;

pub async fn create_generator(
    line: String,
    model: web::Data<Model>,
    generators: web::Data<Generators>,
    config: web::Data<Config>,
) -> IdResponse {
    log::debug!("Creating generator from input {:?}", line);
    let line = filter_input(&line);
    let generator = match model
        .create_token_generator(line, config.generation_options)
        .await
    {
        Ok(generator) => generator,
        Err(e) => {
            log::error!("Failed to create a generator: {}", e);
            return Response::error("failed to create a generator");
        }
    };
    let id = generators.add_state(generator);
    Response::ok(id)
}

pub async fn create_generator_get(
    _auth: Authentication,
    web::Query(q): web::Query<request::Line>,
    model: web::Data<Model>,
    generators: web::Data<Generators>,
    config: web::Data<Config>,
) -> IdResponse {
    let request::Line { line } = q;
    create_generator(line, model, generators, config).await
}

pub async fn create_generator_post(
    _auth: Authentication,
    line: String,
    model: web::Data<Model>,
    generators: web::Data<Generators>,
    config: web::Data<Config>,
) -> IdResponse {
    create_generator(line, model, generators, config).await
}

pub async fn generate<T: Stream<Item = String> + Unpin + ResetGenerator + 'static>(
    _auth: Authentication,
    web::Query(id): web::Query<request::Id>,
    model_states: web::Data<ModelState<T>>,
) -> Either<HttpResponse, StatusResponse> {
    let id = id.id;
    StreamedStateGenerator::from_model_states(&model_states, id)
        .await
        .map_or_else(
            |_| Either::B(StatusResponse::error("wrong id")),
            |stream| {
                let stream = stream
                    .inspect(|t| {
                        log::trace!("Sending GPT output token: {:?}", t);
                    })
                    .map(|t| Bytes::copy_from_slice(t.as_bytes()))
                    .map(Result::<_, ()>::Ok);
                Either::A(HttpResponse::Ok().streaming(stream))
            },
        )
}

pub async fn create_lobby(
    _auth: Authentication,
    web::Query(name): web::Query<request::Name>,
    config: web::Data<Config>,
    lobbies: web::Data<GameLobbies>,
    tokens: web::Data<Tokens>,
) -> TokenResponse {
    for lobby in lobbies.states() {
        let lobby = lobby.lock().await;
        if lobby.name == name.name {
            return Response::error("A lobby with that name already exists");
        }
    }

    let token = crate::state::generate_token(config.get_ref());
    let lobby = GameLobby {
        name: name.name,
        leader_name: None,
        game_create_data: GameCreateData::new(),
        current_game: None,
        ws_connections: Vec::new(),
    };
    log::info!("Created lobby {} with join token {}", lobby.name, token);
    let ref_state = lobbies.add_state(lobby);
    tokens
        .add_lobby_token(Token {
            token: token.clone(),
            expiry: *crate::resources::NO_EXPIRY_TIMESTAMP,
            ref_state,
        })
        .await;

    TokenResponse::ok(token)
}

pub async fn stream(
    web::Query(info): web::Query<request::TokenName>,
    req: HttpRequest,
    stream: web::Payload,
) -> Either<HttpResponse, StatusResponse> {
    let request::TokenName { token, name } = info;
    let conn = match Connection::new(&req, token, name) {
        Some(conn) => conn,
        None => {
            log::info!("Error creating websocket connection");
            return Either::B(StatusResponse::error(
                "internal error creating web socket connection",
            ));
        }
    };
    let resp = ws::start(conn, &req, stream);
    match resp {
        Ok(resp) => {
            log::info!(
                "Incoming web socket connection from {:?} started",
                req.connection_info().realip_remote_addr()
            );
            Either::A(resp)
        }
        Err(e) => {
            log::error!("Error starting web socket connection: {}", e);
            Either::B(StatusResponse::error(
                "internal error creating web socket connection",
            ))
        }
    }
}

fn filter_input(input: &str) -> String {
    let line = input
        .replace('\r', "\n")
        .chars()
        .fold((String::new(), false), |(mut text, is_newline), b| {
            if !is_newline || b != '\n' {
                text.push(b)
            }
            (text, b == '\n')
        })
        .0;
    log::trace!("Filtered input {:?} to {:?}", input, line);
    line
}

#[cfg(test)]
mod test {
    use super::filter_input;

    #[test]
    fn test_filter() {
        assert_eq!(filter_input("Test\r\ntest"), "Test\ntest");
        assert_eq!(filter_input("Test\rtest"), "Test\ntest");
        assert_eq!(filter_input("Test\r\ntest\r\r\r\n"), "Test\ntest\n");
    }
}
