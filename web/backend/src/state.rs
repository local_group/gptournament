pub mod game;
pub mod model_state;
pub mod tokens;

pub use model_state::{Generator, Generators, ModelState, SingleState, StateId};
pub use tokens::{generate_token, Token, Tokens};

pub type GameLobby = SingleState<game::GameLobby>;
pub type GameLobbies = ModelState<game::GameLobby>;
