use crate::{resources::Cleanup, websocket::GameLobbyConnection};
use gpt::{
    game::state::{GameCreateData, GameData},
    model::AsyncModelTokenGenerator,
};
use web_common::types::websocket::Timestamp;

pub struct GameLobby {
    pub name: String,
    pub leader_name: Option<String>,
    pub game_create_data: GameCreateData,
    pub current_game: Option<Game>,
    pub ws_connections: Vec<GameLobbyConnection>,
}

pub struct Game {
    pub game_data: GameData,
    pub feed: Vec<GameFeed>,
    pub current_feed: Option<usize>,
    pub banned_users: Vec<String>,
    pub last_player: usize,
}

pub struct GameFeed {
    pub sequences: Vec<GameFeedSequence>,
    pub continue_until: Timestamp,
    pub user_submission: UserSubmission,
    pub generator: Option<AsyncModelTokenGenerator>,
}

#[derive(Debug, Clone)]
pub struct GameFeedSequence {
    pub text: String,
    pub timestamp: Timestamp,
    pub token_count: u32,
    pub finished: bool,
}

#[derive(Debug, Clone)]
pub struct UserSubmission {
    pub text: String,
    pub user: String,
    pub time: Timestamp,
    pub token: Option<String>,
}

#[async_trait::async_trait(?Send)]
impl Cleanup for GameLobby {
    async fn cleanup(&mut self) {
        let now = chrono::Local::now();
        if let Some(game) = self.current_game.as_mut() {
            for (i, feed) in game.feed.iter_mut().enumerate() {
                if feed.continue_until < now {
                    log::debug!(
                        "Cleaning up generator for game in lobby {}, feed {}",
                        self.name,
                        i
                    );
                    feed.generator.take();
                }
            }
        }
    }
}
