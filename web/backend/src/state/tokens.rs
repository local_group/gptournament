use crate::{
    config::Config,
    resources::{ContainerCleanup, NO_EXPIRY_TIMESTAMP},
    state::{self, game, GameLobbies, SingleState, StateId},
};
use futures_util::lock::Mutex;
use rand::Rng;
use std::collections::HashMap;
use std::sync::{Arc, Weak};

pub struct Token {
    pub token: String,
    pub expiry: chrono::DateTime<chrono::Local>,
    pub ref_state: StateId,
}

struct TokenInternal<T> {
    pub expiry: chrono::DateTime<chrono::Local>,
    pub state: Weak<Mutex<SingleState<T>>>,
}

#[derive(Clone)]
pub struct Tokens {
    lobbies: GameLobbies,
    lobby_tokens: Arc<Mutex<HashMap<String, TokenInternal<game::GameLobby>>>>,
}

impl Tokens {
    pub fn new(lobbies: GameLobbies) -> Self {
        Self {
            lobbies,
            lobby_tokens: Arc::new(Mutex::new(HashMap::new())),
        }
    }

    pub async fn add_lobby_token(&self, token: Token) {
        if let Some(lobby) = self.lobbies.get_inner(token.ref_state).await {
            let token_internal = TokenInternal {
                expiry: token.expiry,
                state: Arc::downgrade(&lobby),
            };
            let mut lobby_tokens = self.lobby_tokens.lock().await;
            lobby_tokens.insert(token.token, token_internal);
        }
    }

    pub async fn get_lobby(&self, token: String) -> Option<Arc<Mutex<state::GameLobby>>> {
        let now = chrono::Local::now();
        let mut lobby_tokens = self.lobby_tokens.lock().await;
        if let Some(lobby_token) = lobby_tokens.get(&token) {
            if lobby_token.expiry != *NO_EXPIRY_TIMESTAMP && lobby_token.expiry < now {
                lobby_tokens.remove(&token);
                return None;
            }
            lobby_token.state.upgrade()
        } else {
            None
        }
    }
}

#[async_trait::async_trait(?Send)]
impl ContainerCleanup for Tokens {
    async fn cleanup(&self) {
        let mut lobby_tokens = self.lobby_tokens.lock().await;
        let now = chrono::Local::now();
        let mut keys = Vec::new();
        for (key, t) in lobby_tokens.iter() {
            if t.expiry != *NO_EXPIRY_TIMESTAMP && t.expiry < now {
                keys.push(key.clone());
            }
        }
        for key in keys {
            log::debug!("Lobby token {} expired", key);
            lobby_tokens.remove(&key);
        }
    }
}

pub fn generate_token(config: &Config) -> String {
    let mut token = String::new();
    let mut rng = rand::thread_rng();
    for _ in 0..config.token_length {
        token.push(rng.gen_range('A'..='Z'));
    }
    token
}
