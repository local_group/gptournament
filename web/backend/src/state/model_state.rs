use crate::resources::{Cleanup, ContainerCleanup};
use futures_util::{lock::Mutex, StreamExt};
use gpt::model::AsyncModelTokenGenerator;
use std::collections::HashMap;
use std::ops::{Deref, DerefMut};
use std::sync::atomic::{AtomicU32, Ordering};
use std::sync::{Arc, RwLock, Weak};
use std::time::{Duration, Instant};

pub type Generator = AsyncModelTokenGenerator;
pub type Generators = ModelState<Generator>;

pub use web_common::types::StateId;

pub struct ModelState<T> {
    inner: Arc<ModelStateImpl<T>>,
}

impl<T> Default for ModelState<T> {
    fn default() -> Self {
        Self::new()
    }
}

impl<T> Clone for ModelState<T> {
    fn clone(&self) -> Self {
        Self {
            inner: Arc::clone(&self.inner),
        }
    }
}

impl<T> ModelState<T> {
    pub fn new() -> Self {
        Self {
            inner: Arc::new(ModelStateImpl::new()),
        }
    }

    pub fn clone(&self) -> Self {
        Self {
            inner: Arc::clone(&self.inner),
        }
    }

    pub fn get_weak(&self) -> Weak<ModelStateImpl<T>> {
        Arc::downgrade(&self.inner)
    }
}

impl<T> Deref for ModelState<T> {
    type Target = ModelStateImpl<T>;

    fn deref(&self) -> &Self::Target {
        &self.inner
    }
}

pub struct ModelStateImpl<T> {
    states: RwLock<HashMap<StateId, Arc<Mutex<SingleState<T>>>>>,
    current_id: AtomicU32,
}

impl<T> Default for ModelStateImpl<T> {
    fn default() -> Self {
        Self::new()
    }
}

impl<T> ModelStateImpl<T> {
    pub fn new() -> Self {
        Self {
            states: RwLock::new(HashMap::new()),
            current_id: AtomicU32::new(0),
        }
    }

    pub async fn get_inner(&self, id: StateId) -> Option<Arc<Mutex<SingleState<T>>>> {
        let arc = {
            let states = self.states.read().unwrap();
            states.get(&id).map(Arc::clone)
        };
        if let Some(arc) = arc {
            {
                let mut lock = arc.lock().await;
                lock.last_use = Instant::now();
            }
            Some(arc)
        } else {
            None
        }
    }

    pub fn add_state<U: Into<T>>(&self, state: U) -> StateId {
        let mut states = self.states.write().unwrap();
        let id = self.current_id.fetch_add(1, Ordering::SeqCst);
        states.insert(
            id,
            Arc::new(Mutex::new(SingleState {
                inner: state.into(),
                last_use: Instant::now(),
            })),
        );
        id
    }

    pub fn states(&self) -> Vec<Arc<Mutex<SingleState<T>>>> {
        let states = self.states.read().unwrap();
        states.values().cloned().collect()
    }
}

#[async_trait::async_trait(?Send)]
impl<T: Cleanup> ContainerCleanup for ModelStateImpl<T> {
    async fn cleanup(&self) {
        let mut cleanup = Vec::new();
        {
            let states = self.states.read().unwrap();
            for (id, state) in states.iter() {
                cleanup.push((*id, Arc::clone(state)));
            }
        }
        for (_, state) in cleanup.iter() {
            let mut state = state.lock().await;
            state.cleanup().await;
        }
        let cleanup = futures_util::stream::iter(cleanup)
            .filter_map(|(id, state)| async move {
                let state = state.lock().await;
                if Instant::now().duration_since(state.last_use) > Duration::from_secs(60 * 10) {
                    Some(id)
                } else {
                    None
                }
            })
            .collect::<Vec<_>>()
            .await;
        if !cleanup.is_empty() {
            log::info!("Cleaning up {} states", cleanup.len());
            let mut states = self.states.write().unwrap();
            for id in cleanup {
                states.remove(&id);
                log::debug!(
                    "Cleaned up state {} with id {}",
                    std::any::type_name::<T>(),
                    id
                );
            }
        }
    }
}

pub struct SingleState<T> {
    inner: T,
    last_use: Instant,
}

impl<T> Deref for SingleState<T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        &self.inner
    }
}

impl<T> DerefMut for SingleState<T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.inner
    }
}
