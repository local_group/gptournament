mod error;
mod macros;
pub mod model;
mod schema;

use crate::{api::helper, config::Config};
pub use error::DBError;
use libreauth::key::KeyBuilder;
use libreauth::pass::HashBuilder;

pub type DbPool = r2d2::Pool<diesel::r2d2::ConnectionManager<diesel::SqliteConnection>>;
type InternalDbConn =
    r2d2::PooledConnection<diesel::r2d2::ConnectionManager<diesel::SqliteConnection>>;

embed_migrations!();

pub struct DbConn(pub(crate) InternalDbConn);

pub trait Model<'a> {
    type Insert;

    fn update(&self, conn: &diesel::SqliteConnection) -> Result<(), DBError>;
    fn add(conn: &diesel::SqliteConnection, value: Self::Insert) -> Result<(), DBError>;
    fn as_insert(&'a self) -> Self::Insert;
}

impl DbConn {
    pub fn update<'a, T: Model<'a>>(&self, data: &T) -> Result<(), DBError> {
        data.update(&self.0)
    }

    pub fn add<'a, T: Model<'a>>(&self, data: &'a T) -> Result<(), DBError> {
        T::add(&self.0, data.as_insert())
    }

    pub fn get_user(&self, name: &str) -> Result<model::User, DBError> {
        model::User::get(&self.0, name.into())
    }

    pub fn get_token(&self, token: &str) -> Result<model::Token, DBError> {
        model::Token::get(&self.0, token.into())
    }

    pub fn get_users(&self) -> Result<Vec<model::User>, DBError> {
        model::User::get_all(&self.0)
    }

    pub fn get(pool: &DbPool) -> Result<Self, DBError> {
        let conn = pool.get()?;
        Ok(Self(conn))
    }
}

pub struct DbSetup {}

impl Default for DbSetup {
    fn default() -> Self {
        Self::new()
    }
}

impl DbSetup {
    pub fn new() -> Self {
        Self {}
    }

    pub fn run_db_setup(&self, config: &Config, db: DbConn) -> Result<(), anyhow::Error> {
        log::info!("Running db migrations");
        if let Err(e) = embedded_migrations::run(&db.0) {
            anyhow::bail!("Error running migrations: {}", e);
        }

        let user = db.get_user("admin");
        if let Ok(mut user) = user {
            match (
                HashBuilder::from_phc(&user.password),
                &config.admin_password,
            ) {
                (Ok(hasher), Some(password)) if !hasher.is_valid(&password) => {
                    log::info!("Updating admin password");
                    match helper::build_hasher().hash(&password) {
                        Ok(hash) => {
                            user.password = hash;
                            if let Err(e) = db.update(&user) {
                                anyhow::bail!("Failed to update admin password: {}", e);
                            }
                        }
                        Err(e) => {
                            anyhow::bail!("Failed to hash admin password: {:?}", e);
                        }
                    }
                }
                (Err(e), _) => {
                    anyhow::bail!("Failed to build hasher: {:?}", e);
                }
                _ => {}
            }
        } else {
            log::info!("No admin user found, creating new admin user");
            let admin_password = match &config.admin_password {
                Some(password) => password.clone(),
                None => {
                    let pass = KeyBuilder::new().size(8).as_hex();
                    log::info!("Generated admin password: {:?}", pass);
                    pass
                }
            };
            let hashed = helper::build_hasher().hash(&admin_password).unwrap();
            let new_user = model::User::new_from_values("admin".into(), hashed, true);
            if let Err(e) = db.add(&new_user) {
                anyhow::bail!("Failed to add admin user: {}", e);
            }
        }

        Ok(())
    }
}
