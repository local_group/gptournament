use crate::state::ModelState;
use gpt::model::{AsyncModel, AsyncModelTokenGenerator};
use std::ops::Deref;
use std::sync::{Arc, Weak};
use std::time::Duration;
use tokio::task::{JoinHandle, LocalSet};

lazy_static::lazy_static! {
    pub static ref NO_EXPIRY_TIMESTAMP: chrono::DateTime<chrono::Local> = chrono::MIN_DATETIME.with_timezone(&chrono::Local);
}

#[async_trait::async_trait(?Send)]
pub trait Cleanup {
    async fn cleanup(&mut self) {}
}

#[async_trait::async_trait(?Send)]
pub trait ContainerCleanup {
    async fn cleanup(&self) {}
}

#[derive(Clone)]
pub struct Model {
    model: Arc<AsyncModel>,
}

impl Model {
    pub fn new(model: AsyncModel) -> Self {
        Self {
            model: Arc::new(model),
        }
    }
}

impl Deref for Model {
    type Target = AsyncModel;

    fn deref(&self) -> &Self::Target {
        &*self.model
    }
}

pub struct StateCleanup {
    local: LocalSet,
    handles: Vec<JoinHandle<()>>,
}

impl Default for StateCleanup {
    fn default() -> Self {
        Self::new()
    }
}

impl StateCleanup {
    pub fn new() -> Self {
        Self {
            local: LocalSet::new(),
            handles: Vec::new(),
        }
    }

    pub async fn run(self) {
        self.local.await;
        for handle in self.handles {
            let _ = handle.await;
        }
    }

    pub fn add_container<T: 'static + ContainerCleanup>(&mut self, container: T) {
        self.handles.push(
            self.local
                .spawn_local(Self::container_cleanup_task(container)),
        );
    }

    pub fn add_state<T: 'static + Cleanup>(&mut self, state: ModelState<T>) {
        let weak = state.get_weak();
        self.handles.push(
            self.local
                .spawn_local(Self::weak_container_cleanup_task(weak)),
        );
    }

    async fn container_cleanup_task<C: ContainerCleanup>(container: C) {
        log::info!("Starting cleanup thread");
        let mut interval = tokio::time::interval(Duration::from_secs(60));
        loop {
            interval.tick().await;
            log::debug!("Running cleanup");
            container.cleanup().await;
        }
    }

    async fn weak_container_cleanup_task<C: ContainerCleanup>(container: Weak<C>) {
        log::info!("Starting cleanup thread");
        let mut interval = tokio::time::interval(Duration::from_secs(60));
        loop {
            interval.tick().await;
            if let Some(container) = container.upgrade() {
                log::debug!("Running cleanup");
                container.cleanup().await;
            } else {
                break;
            }
        }
    }
}

#[async_trait::async_trait(?Send)]
impl Cleanup for AsyncModelTokenGenerator {}
