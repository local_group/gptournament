use crate::{
    api::helper::cookie::{CookieBuilder, Cookies},
    error::GuardError,
};
use actix_web::{
    dev::{MessageBody, Service, ServiceRequest, ServiceResponse, Transform},
    web::Data,
    HttpMessage,
};
use cookie::CookieJar;
use futures_util::{
    future,
    task::{Context, Poll},
    FutureExt,
};
use std::pin::Pin;

pub struct CookieMiddleware {}

impl Default for CookieMiddleware {
    fn default() -> Self {
        Self::new()
    }
}

impl CookieMiddleware {
    pub fn new() -> Self {
        Self {}
    }
}

impl<S, B> Transform<S> for CookieMiddleware
where
    S: Service<Request = ServiceRequest, Response = ServiceResponse<B>, Error = actix_web::Error>,
    B: MessageBody,
{
    type Request = ServiceRequest;
    type Response = ServiceResponse<B>;
    type Error = actix_web::Error;
    type Transform = CookieMiddlewareImpl<S>;
    type InitError = ();
    type Future = future::Ready<Result<Self::Transform, Self::InitError>>;

    fn new_transform(&self, service: S) -> Self::Future {
        future::ok(CookieMiddlewareImpl { service })
    }
}

pub struct CookieMiddlewareImpl<S> {
    service: S,
}

impl<S, B> Service for CookieMiddlewareImpl<S>
where
    S: Service<Request = ServiceRequest, Response = ServiceResponse<B>, Error = actix_web::Error>,
    B: MessageBody,
{
    type Request = ServiceRequest;
    type Response = ServiceResponse<B>;
    type Error = actix_web::Error;
    type Future = future::Either<
        CookieMiddlewareResponse<S, B>,
        future::Ready<Result<Self::Response, Self::Error>>,
    >;

    fn poll_ready(&mut self, ctx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        self.service.poll_ready(ctx)
    }

    fn call(&mut self, req: Self::Request) -> Self::Future {
        let result = req
            .app_data::<Data<CookieBuilder>>()
            .ok_or(GuardError::InternalServerError.into());
        let builder = match result {
            Ok(result) => result,
            Err(e) => return future::err(e).right_future(),
        };
        log::trace!("Loading cookies from http request");
        let cookies = match req.cookies() {
            Ok(cookies) => cookies,
            Err(e) => return future::err(e.into()).right_future(),
        };
        let mut jar = CookieJar::new();
        for cookie in cookies.iter() {
            jar.add_original(cookie.clone());
        }
        drop(cookies);
        let cookies = builder.new_from_jar(jar);
        req.extensions_mut().insert(cookies);
        log::trace!("Cookie store inserted into request");

        CookieMiddlewareResponse {
            future: self.service.call(req),
            _t: std::marker::PhantomData,
        }
        .left_future()
    }
}

#[pin_project::pin_project]
pub struct CookieMiddlewareResponse<S, B>
where
    S: Service,
    B: MessageBody,
{
    #[pin]
    future: S::Future,
    _t: std::marker::PhantomData<B>,
}

impl<S, B> futures_util::future::Future for CookieMiddlewareResponse<S, B>
where
    S: Service<Request = ServiceRequest, Response = ServiceResponse<B>, Error = actix_web::Error>,
    B: MessageBody,
{
    type Output = Result<ServiceResponse<B>, actix_web::Error>;

    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let this = self.project();

        let mut res: S::Response = match this.future.poll(cx) {
            Poll::Ready(Ok(res)) => res,
            Poll::Ready(Err(e)) => return Poll::Ready(Err(e)),
            Poll::Pending => return Poll::Pending,
        };

        log::trace!("Inserting cookie delta into response");
        let cookies = res.request().extensions().get::<Cookies>().unwrap().clone();
        cookies.insert_into_headers(res.headers_mut());
        Poll::Ready(Ok(res))
    }
}
