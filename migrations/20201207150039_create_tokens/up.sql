CREATE TABLE tokens
(
    id            INTEGER PRIMARY KEY,
    token         VARCHAR NOT NULL,
    creating_user VARCHAR NOT NULL,
    expiry        TEXT NOT NULL
);
