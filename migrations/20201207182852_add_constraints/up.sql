ALTER TABLE users
    RENAME TO old_users;
CREATE TABLE users
(
    id       INTEGER PRIMARY KEY,
    name     VARCHAR NOT NULL,
    password VARCHAR NOT NULL,
    is_admin BOOLEAN NOT NULL,
    CONSTRAINT unique_name UNIQUE (name)
);
INSERT INTO users
SELECT *
FROM old_users;
DROP TABLE old_users;

ALTER TABLE tokens
    RENAME TO old_tokens;
CREATE TABLE tokens
(
    id            INTEGER PRIMARY KEY,
    token         VARCHAR NOT NULL,
    creating_user VARCHAR NOT NULL,
    expiry        TEXT    NOT NULL,
    CONSTRAINT unique_token UNIQUE (token)
);
INSERT INTO tokens
SELECT *
FROM old_tokens;
DROP TABLE old_tokens;
